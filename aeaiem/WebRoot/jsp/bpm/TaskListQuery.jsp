<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>任务列表</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
var processBox;
function viewTaskDetail(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;		
	}
	if (!processBox){
		processBox = new PopupBox('processBox','业务流程操作',{size:'normal',width:'900px',height:'700px',top:'3px',scroll:'yes'});
	}
	var url = $("#bizFormURL").val();
	processBox.sendRequest(url);
}
var instanceGraphBox;
function viewInstanceGraphBox(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	postRequest('form1',{actionType:'retrieveIds',onComplete:function(responseText){
		var json = $.parseJSON(responseText);	
		var WFP_ID = json.processId;			
		var WFIP_ID = json.processInstId;
		var BpmShowFlowUrl = json.BpmShowFlowUrl;
	if (!instanceGraphBox){
			instanceGraphBox = new PopupBox('instanceGraphBox','业务流程图',{size:'normal',width:'900px',height:'500px',top:'3px',scroll:'yes'});
		}
		var url = BpmShowFlowUrl+"&actionType=prepareDisplay&WFIP_ID="+WFIP_ID+"&WFP_ID="+WFP_ID;
		instanceGraphBox.sendRequest(url);			
	}});
}
</script>
</head>
<body>
<div style="height: 5px"></div>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post" >
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">
<input name="userId" type="hidden" id="userId" value="<%=pageBean.inputValue("userId")%>" />
</div>
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doSubmit('prepareDisplay')"><input value="&nbsp;" title="刷新" type="button" class="refreshImgBtn" />刷新</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="viewInstanceGraphBox('')"><input value="&nbsp;" type="button" class="flowImgBtn" id="flowImgBtn" title="流程图" />流程图</td>
</tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="任务列表.csv"
retrieveRowsCallback="process" xlsFileName="任务列表.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="10"
listWidth="100%" 
height="400px" 
>
<ec:row styleClass="odd" ondblclick="viewTaskDetail()" onclick="selectRow(this,{bizFormURL:'${row.WFIP_BIZURL}',processId:'${row.WFP_ID}',processInstId:'${row.WFIP_ID}',bizRecordId:'${row.WFIP_BUSINESS_ID}',activityCode:'${row.WFA_CODE}'});">
	<ec:column width="30" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="WFP_NAME" title="流程名称"   />
	<ec:column width="80" property="WFA_NAME" title="节点名称"   />
	<ec:column width="270" property="WFIP_TITLE" title="业务标题"   />
	<ec:column width="80" property="WFIP_LAUCHER_NAME" title="发起人"   />
	<ec:column width="100" property="WFIP_LAUCHER_TIME" title="发起时间"  cell="date" format="yyyy-MM-dd HH:mm" />	
	<ec:column width="80" property="WFIP_OPERATER_NAME" title="提交人"   />
	<ec:column width="100" property="WFIP_OPERATER_TIME" title="提交时间"  cell="date" format="yyyy-MM-dd HH:mm" />
</ec:row>
</ec:table>
<input type="hidden" name="bizFormURL" id="bizFormURL" value="" />
<input type="hidden" name="bizRecordId" id="bizRecordId" value="" />
<input type="hidden" name="processId" id="processId" value="" />
<input type="hidden" name="activityCode" id="activityCode" value="" />
<input type="hidden" name="processInstId" id="processInstId" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('bizRecordId');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
