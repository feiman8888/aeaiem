<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>项目配置</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function bao(pcStyle){
	doSubmit({actionType:'showPcCode',doValidate:'false'});
}
function save(actionType){
	if(actionType==save){
		doSubmit({actionType:'save'})
	}
}
function changeStateRecord(actionType){
	doSubmit({actionType:actionType});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%if(pageBean.getBoolValue("showEdit")){ %>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="save(save)"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
<%} %>   
<%if(pageBean.getBoolValue("showSubmit")){ %>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="changeStateRecord('submit')"><input value="&nbsp;" type="button" class="submitImgBtn" id="submitImgBtn" title="提交" />提交</td>
<%} %>   
<%if(pageBean.getBoolValue("showResubmit")){ %>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="changeStateRecord('resubmit')"><input value="&nbsp;" type="button" class="resubmitImgBtn" id="resubmitImgBtn" title="反提交" />反提交</td>
<%} %>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>项目类型</th>
	<td><select id="PC_STYLE" label="项目类型" name="PC_STYLE" class="select" onchange="bao()" 
	<%if(!pageBean.getBoolValue("canEdit")){ %> disabled="disabled" <%}%> >
	<%=pageBean.selectValue("PC_STYLE")%></select>
	<input id="PC_STYLE_READ" label="项目类型" name="PC_STYLE_READ" type="hidden" value="<%=pageBean.inputValue("PC_STYLE_READ")%>" />
</td>
</tr>
<tr>
	<th width="100" nowrap>项目编号</th>
	<td><input id="PC_CODE" label="项目编号" name="PC_CODE" type="text" value="<%=pageBean.inputValue("PC_CODE")%>" readonly="readonly" size="41" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>项目名称</th>
	<td><input id="PC_NAME" label="项目名称" name="PC_NAME" type="text" value="<%=pageBean.inputValue("PC_NAME")%>" size="41" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>项目地点</th>
	<td><input id="PC_LOCATION" label="项目地点" name="PC_LOCATION" type="text" value="<%=pageBean.inputValue("PC_LOCATION")%>" size="41" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>甲方</th>
	<td><input id="PC_FIRST_PARTY" label="甲方" name="PC_FIRST_PARTY" type="text" value="<%=pageBean.inputValue("PC_FIRST_PARTY")%>" size="41" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>甲方负责人</th>
	<td><input id="PC_FIRST_PARTY_HEAD" label="甲方负责人" name="PC_FIRST_PARTY_HEAD" type="text" value="<%=pageBean.inputValue("PC_FIRST_PARTY_HEAD")%>" size="41" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>实施负责人</th>
	<td><input id="PC_RESPONSIBLE_PERSON" label="实施负责人" name="PC_RESPONSIBLE_PERSON" type="text" value="<%=pageBean.inputValue("PC_RESPONSIBLE_PERSON")%>" size="41" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>销售负责人</th>
	<td><input id="PC_SALES_PERSON" label="销售负责人" name="PC_SALES_PERSON" type="text" value="<%=pageBean.inputValue("PC_SALES_PERSON")%>" size="41" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>项目金额</th>
	<td><input id="PC_MONEY" label="项目金额" name="PC_MONEY" type="text" value="<%=pageBean.inputValue("PC_MONEY")%>" size="41" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>开始日期</th>
	<td><input id="PC_START_DATE" label="开始日期" name="PC_START_DATE" type="text" value="<%=pageBean.inputDate("PC_START_DATE")%>" size="41" class="text" readonly="readonly" /><img id="PC_START_DATEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
<tr>
	<th width="100" nowrap>结束日期</th>
	<td><input id="PC_END_DATE" label="结束日期" name="PC_END_DATE" type="text" value="<%=pageBean.inputDate("PC_END_DATE")%>" size="41" class="text" readonly="readonly" /><img id="PC_END_DATEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
<tr>
	<th width="100" nowrap>项目状态</th>
	<td>
		<input id="PC_STATE_TEXT" label="项目状态" name="PC_STATE_TEXT" type="text" value="<%=pageBean.selectedText("PC_STATE")%>" readonly="readonly" size="41" class="text" />
		<input id="PC_STATE" label="项目状态" name="PC_STATE" type="hidden" value="<%=pageBean.selectedValue("PC_STATE")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>备注</th>
	<td><textarea id="PC_REMARKS" label="备注" name="PC_REMARKS" cols="60" rows="5" class="textarea"><%=pageBean.inputValue("PC_REMARKS")%></textarea>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="PC_ID" name="PC_ID" value="<%=pageBean.inputValue4DetailOrUpdate("PC_ID","")%>" />
</form>
<script language="javascript">
initCalendar('PC_START_DATE','%Y-%m-%d','PC_START_DATEPicker');
initCalendar('PC_END_DATE','%Y-%m-%d','PC_END_DATEPicker');
datetimeValidators[0].set("yyyy-MM-dd").add("PC_START_DATE");
datetimeValidators[1].set("yyyy-MM-dd").add("PC_END_DATE");
requiredValidator.add("PC_CODE");
requiredValidator.add("PC_NAME");
requiredValidator.add("PC_STYLE");
requiredValidator.add("PC_LOCATION");
requiredValidator.add("PC_FIRST_PARTY");
requiredValidator.add("PC_SALES_PERSON");
requiredValidator.add("PC_START_DATE");
numValidator.add("PC_MONEY");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
