<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>费用报销审批</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<style type="text/css">
.markable{
	background-color:yellow;
	color:black;
}
</style>
<script language="javascript">
function changeStateRecord(actionType){
	if(confirm("您确认要提交这条记录吗？")) {
		var appOpinion = $("#EXP_APP_OPINION").val();
		var appResult = $('#form1 input[name="EXP_APP_RESULT"]:checked ').val();
		if('N'==appResult && appOpinion.length==0){
			alert("审批意见不能为空！");
		}else{
			postRequest('form1',{'actionType':actionType,onComplete:function(responseText){
				if (responseText == 'success'){
					parent.window.location.reload();
				}
			}});
		}
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<%if(!pageBean.getBoolValue("onlyShowInfo")){ %>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="changeStateRecord('submit')"><input value="&nbsp;" type="button" class="submitImgBtn" id="submitImgBtn" title="提交" />提交</td>
   	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
</tr>
</table>
</div>
<div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>状态</th>
	<td>
	<input id="ER_STATE_TEXT" label="状态" class="text markable" name="ER_STATE_TEXT" type="text" readonly="readonly" value="<%=pageBean.selectedText("ER_STATE")%>" /> 
	<input id="ER_STATE" label="状态" name="ER_STATE" type="hidden" value="<%=pageBean.selectedValue("ER_STATE")%>" />
</td>
</tr>
<tr>
<th width="100" nowrap>操作类型</th>
	<td>&nbsp;<%=pageBean.selectRadio("EXP_APP_RESULT")%>
</td>
</tr>
<tr>
	<th width="100" nowrap>审批意见</th>
	<td colspan="3"><textarea id="EXP_APP_OPINION" label="审批意见" name="EXP_APP_OPINION" cols="105%" rows="5"  style="width:99%" class="textarea"><%=pageBean.inputValue("EXP_APP_OPINION")%></textarea>
</td>
</tr>
</table>
</div>
<%} %>
<span class="titleLabel">表单数据</span>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>编码</th>
	<td><input id="ER_CODE" label="编码" name="ER_CODE" type="text" value="<%=pageBean.inputValue("ER_CODE")%>" size="41" class="text" readonly="readonly"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>标题</th>
	<td><input id="ER_TITLE" label="标题" name="ER_TITLE" type="text" value="<%=pageBean.inputValue("ER_TITLE")%>" size="30" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>报销人</th>
	<td>
	<input id="ER_PERSON_NAME" label="报销人" name="ER_PERSON_NAME" type="text" value="<%=pageBean.inputValue("ER_PERSON_NAME")%>" readonly="readonly" size="30" class="text" />
	<input id="ER_PERSON" label="报销人" name="ER_PERSON" type="hidden" value="<%=pageBean.inputValue("ER_PERSON")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>报销日期</th>
	<td><input id="ER_DATE" label="报销日期" name="ER_DATE" type="text" value="<%=pageBean.inputDate("ER_DATE")%>" readonly="readonly" size="30" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>报销金额</th>
	<td><input id="ER_MONEY" label="报销金额" name="ER_MONEY" class="text markable" type="text" value="<%=pageBean.inputValue("ER_MONEY")%>" size="30" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>报销类型</th>
	<td><select id="ER_STYLE" label="报销类型" name="ER_STYLE" class="select"><%=pageBean.selectValue("ER_STYLE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>说明</th>
	<td><textarea id="ER_ILLUSTRATE" label="说明" name="ER_ILLUSTRATE" cols="105%" rows="5" class="textarea"><%=pageBean.inputValue("ER_ILLUSTRATE")%></textarea>
</td>
</tr>
<tr>
	<th width="100" nowrap>备注</th>
	<td><textarea id="ER_REMARKS" label="备注" name="ER_REMARKS" cols="105%" rows="5" class="textarea"><%=pageBean.inputValue("ER_REMARKS")%></textarea>
</td>
</tr>
</table>

<%
List param0Records = (List)pageBean.getAttribute("ApproceOpinionRecords");
pageBean.setRsList(param0Records);
%>
<span class="titleLabel">审批记录</span>
<table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable ecSide" id="dataTable"  style="table-layout:fixed;word-break:break-all;">
<thead>
  <tr>
    <th width="20" align="center" nowrap="nowrap">序号</th>
    <th width="100" align="center">审批人</th>
    <th width="80" align="center">审批结果</th>
    <th width="100" align="center">审批意见</th>
    <th width="100" align="center">审批时间</th>
  </tr>
</thead>
<tbody>
<%
int paramSize = pageBean.listSize();
for (int i=0;i < paramSize;i++){
%>
  <tr onmouseout="ECSideUtil.unlightRow(this);" onmouseover="ECSideUtil.lightRow(this);" onclick="ECSideUtil.selectRow(this,'form1');selectRow(this,{currentRecordIndex:'<%=i%>'})">
	<td style="text-align:center"><%=i+1%></td>
    <td><%=pageBean.inputValue(i,"EXP_APP_PERSON_NAME")%>
    </td>
    <td><%=pageBean.inputValue(i,"EXP_APP_RESULT_NAME")%>
    </td>
    <td><%=pageBean.inputValue(i,"EXP_APP_OPINION")%>
    </td>
    <td><%=pageBean.inputValue(i,"EXP_APP_TIME")%>
    </td>
<%}%>
</tbody>
</table>

<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="ER_ID" name="ER_ID" value="<%=pageBean.inputValue("ER_ID")%>" />
<input type="hidden" id="PC_ID" name="PC_ID" value="<%=pageBean.inputValue("PC_ID")%>" />
<input type="hidden" id="WFIP_BUSINESS_ID" name="WFIP_BUSINESS_ID" value="<%=pageBean.getAttribute("WFIP_BUSINESS_ID")%>" />
<input type="hidden" id="WFA_CODE" name="WFA_CODE" value="<%=pageBean.getAttribute("WFA_CODE")%>" />
<input type="hidden" id="WFIP_ID" name="WFIP_ID" value="<%=pageBean.getAttribute("WFIP_ID")%>" />
<input type="hidden" id="WFP_ID" name="WFP_ID" value="<%=pageBean.getAttribute("WFP_ID")%>" />
</form>
<script language="javascript">
requiredValidator.add("ER_TITLE");
requiredValidator.add("ER_PERSON");
requiredValidator.add("ER_DATE");
datetimeValidators[0].set("yyyy-MM-dd").add("ER_DATE");
requiredValidator.add("ER_MONEY");
numValidator.add("ER_MONEY");
requiredValidator.add("ER_STYLE");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
