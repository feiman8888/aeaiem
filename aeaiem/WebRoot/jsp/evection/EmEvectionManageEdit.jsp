<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String currentSubTableId = pageBean.getStringValue("currentSubTableId");
String currentSubTableIndex = pageBean.getStringValue("currentSubTableIndex");
%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>出差报销</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<style type="text/css">
.markable{
	background-color:yellow;
	color:black;
}
</style>
<script language="javascript">
function saveMasterRecord(){
	if (validate()){
		if (ele("currentSubTableId")){
			var subTableId = $("#currentSubTableId").val();
			if (!checkEntryRecords(subTableId)){
				return;
			}
		}
		showSplash();
		postRequest('form1',{actionType:'saveMasterRecord',onComplete:function(responseText){
			if ("fail" != responseText){
				$('#operaType').val('update');
				$('#EVE_ID').val(responseText);
				doSubmit({actionType:'prepareDisplay'});
			}else{
				hideSplash();
				writeErrorMsg('保存操作出错啦！');
			}
		}});
	}
}
function changeSubTable(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'prepareDisplay'});
}
function refreshPage(){
	doSubmit({actionType:'changeSubTable'});
}
function addEntryRecord(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'addEntryRecord'});
}
function deleteEntryRecord(subTableId){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (confirm('确认要删除该条记录吗？')){
		$('#currentSubTableId').val(subTableId);
		doSubmit({actionType:'deleteEntryRecord'});	
	}
}
function checkEntryRecords(subTableId){
	var result = true;
	var currentRecordSize = $('#currentRecordSize').val();
	return result;
}
var insertSubRecordBox;
function insertSubRecordRequest(title,handlerId){
	if (!insertSubRecordBox){
		insertSubRecordBox = new PopupBox('insertSubRecordBox',title,{size:'normal',height:'400px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType=insert&EVE_ID='+$('#EVE_ID').val();
	insertSubRecordBox.sendRequest(url);	
}
var copySubRecordBox;
function copySubRecordRequest(title,handlerId,subPKField){
	clearSelection();
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!copySubRecordBox){
		copySubRecordBox = new PopupBox('copySubRecordBox',title,{size:'normal',height:'400px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType=copy&'+subPKField+'='+$("#"+subPKField).val();
	copySubRecordBox.sendRequest(url);	
}
var viewSubRecordBox;
function viewSubRecordRequest(operaType,title,handlerId,subPKField){
	clearSelection();
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!viewSubRecordBox){
		viewSubRecordBox = new PopupBox('viewSubRecordBox',title,{size:'normal',height:'400px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType='+operaType+'&'+subPKField+'='+$("#"+subPKField).val();
	viewSubRecordBox.sendRequest(url);
}
function deleteSubRecord(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (confirm('确认要删除该条记录吗？')){
		doSubmit({actionType:'deleteSubRecord'});	
	}
}
function doMoveUp(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	doSubmit({actionType:'moveUp'});
}
function doMoveDown(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}	
	doSubmit({actionType:'moveDown'});
}
var pcIdBox;
function openPcIdBox(){
	var handlerId = "QueryProListSelectList"; 
	if (!pcIdBox){
		pcIdBox = new PopupBox('pcIdBox','请选择项目',{size:'normal',width:'400',top:'2px'});
	}
	var url = 'index?'+handlerId+'&targetId=PC_ID&targetName=PC_NAME';
	pcIdBox.sendRequest(url);
} 
function changeStateRecord(actionType){
	if(confirm("您确认要提交这条记录吗？")) {
		postRequest('form1',{'actionType':actionType,onComplete:function(responseText){
			if (responseText == 'success'){
				location.reload([false]);
			}
			if(responseText == 'nosub'){
				writeErrorMsg('请录入费用清单后再尝试提交!');
			}
		}});
	}
}
var instanceGraphBox;
function viewInstanceGraphBox(){
	postRequest('form1',{actionType:'retrieveIds',onComplete:function(responseText){
			var json = $.parseJSON(responseText);	
			var WFP_ID = json.processId;			
			var WFIP_ID = json.processInstId;
			var BpmShowFlowUrl = json.BpmShowFlowUrl;
		if (!instanceGraphBox){
				instanceGraphBox = new PopupBox('instanceGraphBox','业务流程图',{size:'normal',width:'900px',height:'500px',top:'3px',scroll:'yes'});
			}
			var url = BpmShowFlowUrl+"&actionType=prepareDisplay&WFIP_ID="+WFIP_ID+"&WFP_ID="+WFP_ID;
			instanceGraphBox.sendRequest(url);			
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div style="padding-top:7px;">
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div class="photobg1" id="tabHeader">
<div class="newarticle1" onclick="changeSubTable('_base')">基础信息</div>
<%if (!"insert".equals(pageBean.getOperaType())){%>
 <div class="newarticle1" onclick="changeSubTable('EmExpenses')">费用清单</div>
<%}%>
</div>
<div class="photobox newarticlebox" id="Layer0" style="height:430px;">
<div style="margin:2px;">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%if(pageBean.getBoolValue("showEdit")){ %>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveMasterRecord();"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
<%}%>
<%if(pageBean.getBoolValue("showSubmit")){ %>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="changeStateRecord('submit')"><input value="&nbsp;" type="button" class="submitImgBtn" id="submitImgBtn" title="提交" />提交</td>
<%}%>   
<%if(pageBean.getBoolValue("showFlow")){ %>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="viewInstanceGraphBox('')"><input value="&nbsp;" type="button" class="flowImgBtn" id="flowImgBtn" title="流程图" />流程图</td>
<% }%>
<%if(!"BPM".equals(pageBean.getStringValue("invokeFrom"))){ %>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
<% }%>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
</tr>
<%if(pageBean.getBoolValue("showCode")){ %>
<tr>
	<th width="100" nowrap>编码</th>
	<td><input id="EVE_CODE" label="编码" name="EVE_CODE" type="text" value="<%=pageBean.inputValue("EVE_CODE")%>" size="41" class="text" readonly="readonly"/>
</td>
</tr>
<%} %>
<tr>
	<th width="100" nowrap>项目名称</th>
	<td>
	<input id="PC_NAME" label="项目名称" name="PC_NAME" type="text" value="<%=pageBean.inputValue("PC_NAME")%>" readonly="readonly" size="41" class="text" ondblclick="emptyText('PC_NAME')" />
	<input type="hidden" label="项目标识" id="PC_ID" name="PC_ID" value="<%=pageBean.inputValue("PC_ID")%>" />
	<img id="pcIdSelectImage" src="images/sta.gif" width="16" height="16" onclick="openPcIdBox()" />
</td>
</tr>
<tr>
	<th width="100" nowrap>报销时间</th>
	<td><input id="EVE_REIMBURSEMENT_TIME" label="报销时间" name="EVE_REIMBURSEMENT_TIME" type="text" value="<%=pageBean.inputDate("EVE_REIMBURSEMENT_TIME")%>" readonly="readonly" size="41" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>出差人</th>
	<td>
	<input id="EVE_APPLICATION_NAME" label="出差人" name="EVE_APPLICATION_NAME" type="text" value="<%=pageBean.inputValue("EVE_APPLICATION_NAME")%>" readonly="readonly" size="41" class="text" />
	<input id="EVE_APPLICATION" label="出差人" name="EVE_APPLICATION" type="hidden" value="<%=pageBean.inputValue("EVE_APPLICATION")%>"  />
</td>
</tr>
<tr>
	<th width="100" nowrap>同行人</th>
	<td><input id="EVE_TOGETHER" label="同行人" name="EVE_TOGETHER" type="text" value="<%=pageBean.inputValue("EVE_TOGETHER")%>" size="41" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>开始日期</th>
	<td><input id="EVE_START_TIME" label="开始日期" name="EVE_START_TIME" type="text" value="<%=pageBean.inputDate("EVE_START_TIME")%>" size="41" class="text" /><img id="EVE_START_TIMEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
<tr>
	<th width="100" nowrap>结束日期</th>
	<td><input id="EVE_OVER_TIME" label="结束日期" name="EVE_OVER_TIME" type="text" value="<%=pageBean.inputDate("EVE_OVER_TIME")%>" size="41" class="text" /><img id="EVE_OVER_TIMEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
<tr>
	<th width="100" nowrap>出差天数</th>
	<td><input id="EVE_DAYS" label="出差天数" name="EVE_DAYS" type="text" value="<%=pageBean.inputValue("EVE_DAYS")%>" size="41" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>补助金额</th>
	<td><input id="EVE_SUBSIDY" label="补助金额" name="EVE_SUBSIDY" type="text" value="<%=pageBean.inputValue("EVE_SUBSIDY")%>" size="41" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>汇总费用</th>
	<td><input id="EVE_TOTAL_MONEY" label="汇总费用" name="EVE_TOTAL_MONEY" class="text markable" type="text" value="<%=pageBean.inputValue("EVE_TOTAL_MONEY")%>" readonly="readonly" size="41" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td>
		<input id="EVE_STATE_TEXT" label="状态" name="EVE_STATE_TEXT" type="text" value="<%=pageBean.selectedText("EVE_STATE")%>" readonly="readonly" size="41" class="text" />
		<input id="EVE_STATE" label="状态" name="EVE_STATE" type="hidden" value="<%=pageBean.selectedValue("EVE_STATE")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>出差事由</th>
	<td>
	<textarea id="EVE_REASON" label="出差事由" name="EVE_REASON" cols="60" rows="5"  class="textarea"><%=pageBean.inputValue("EVE_REASON")%></textarea>
</td>
</tr>
</table>
</div>
<%if(pageBean.getBoolValue("hasAppRecords")){ %>
<div id="approveList">
<%
List param0Records = (List)pageBean.getAttribute("ApproceOpinionRecords");
pageBean.setRsList(param0Records);
%>
<table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable ecSide" id="dataTable"  style="table-layout:fixed;word-break:break-all;">
<thead>
  <tr>
    <th width="20" align="center" nowrap="nowrap">序号</th>
    <th width="100" align="center">审批人</th>
    <th width="80" align="center">审批结果</th>
    <th width="100" align="center">审批意见</th>
    <th width="100" align="center">审批时间</th>
  </tr>
</thead>
<tbody>
<%
int paramSize = pageBean.listSize();
for (int i=0;i < paramSize;i++){
%>
  <tr onmouseout="ECSideUtil.unlightRow(this);" onmouseover="ECSideUtil.lightRow(this);" onclick="ECSideUtil.selectRow(this,'form1');selectRow(this,{currentRecordIndex:'<%=i%>'})">
	<td style="text-align:center"><%=i+1%></td>
    <td><%=pageBean.inputValue(i,"EVE_APP_PERSON_NAME")%>
    </td>
    <td><%=pageBean.inputValue(i,"EVE_APP_RESULT_NAME")%>
    </td>
    <td><%=pageBean.inputValue(i,"EVE_APP_OPINION")%>
    </td>
    <td><%=pageBean.inputValue(i,"EVE_APP_TIME")%>
    </td>
<%}%>
</tbody>
</table>
</div>
<%} %>
</div>
<%if (!"insert".equals(pageBean.getOperaType())){%>
<%if ("EmExpenses".equals(currentSubTableId)){ %>
<div class="photobox newarticlebox" id="Layer2" style="height:auto;">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%if(pageBean.getBoolValue("showEdit")){ %>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="insertSubRecordRequest('费用清单','EmExpensesEditBox')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="copySubRecordRequest('费用清单','EmExpensesEditBox','EXP_ID')"><input value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="viewSubRecordRequest('update','费用清单','EmExpensesEditBox','EXP_ID')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>      
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="deleteSubRecord()"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
<%} %>
</tr>   
   </table>
</div>
<div style="margin:2px;">
<%
List param1Records = (List)pageBean.getAttribute("EmExpensesRecords");
pageBean.setRsList(param1Records);
%>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="费用清单.csv"
retrieveRowsCallback="process" xlsFileName="费用清单.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize|export|extend|status"
width="100%" rowsDisplayed="10"
listWidth="100%" 
height="auto"
>
<ec:row styleClass="odd" ondblclick="viewSubRecordRequest('detail','费用清单','EmExpensesEditBox','EXP_ID')" onclick="selectRow(this,{EXP_ID:'${row.EXP_ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="EXP_DEPARTURE" title="出发地点"   />
	<ec:column width="100" property="EXP_DESTINATION" title="目的地点"   />
	<ec:column width="100" property="EXP_START_TIME" title="出发时间" cell="date" format="yyyy-MM-dd HH:mm" />
	<ec:column width="100" property="EXP_END_TIME" title="到达时间" cell="date" format="yyyy-MM-dd HH:mm" />
	<ec:column width="100" property="EXP_TRANSPORTATION_WAY" title="交通方式"  mappingItem="EVP_TRANSPORTATION_WAY" />
	<ec:column width="100" property="EXP_TRANSPORTATION_FEE" title="交通费用"   />
	<ec:column width="100" property="EXP_HOREL_FEE" title="住宿费用"   />
	<ec:column width="100" property="EXP_OTHER_FEE" title="其他费用"   />
	<ec:column width="100" property="EXP_REMARKS" title="备注"   />
</ec:row>
</ec:table>
</div>
</div>
<input type="hidden" name="EXP_ID" id="EXP_ID" value=""/>
<script language="javascript">
setRsIdTag('EXP_ID');
</script>
<%}%>


<input type="hidden" id="currentSubTableId" name="currentSubTableId" value="<%=pageBean.inputValue("currentSubTableId")%>" />
<%if (!"_base".equals(pageBean.inputValue("currentSubTableId"))){%>
<script language="javascript">
$("#Layer0").hide();
</script>
<%}%>
<%}%>
<script language="javascript">
new Tab('tab','tabHeader','Layer',<%=currentSubTableIndex%>);
<%if (!"_base".equals(pageBean.inputValue("currentSubTableId"))){%>
$("#Layer0").hide();
<%}%>
</script>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" name="invokeFrom" id="invokeFrom" value="<%=pageBean.inputValue("invokeFrom")%>"/>
<input type="hidden" id="EVE_ID" name="EVE_ID" value="<%=pageBean.inputValue("EVE_ID")%>" />
</div>
</form>
<script language="javascript">
<%if ("_base".equals(pageBean.inputValue("currentSubTableId"))){%>
initCalendar('EVE_START_TIME','%Y-%m-%d','EVE_START_TIMEPicker');
initCalendar('EVE_OVER_TIME','%Y-%m-%d','EVE_OVER_TIMEPicker');
requiredValidator.add("PC_ID");
requiredValidator.add("EVE_START_TIME");
datetimeValidators[0].set("yyyy-MM-dd").add("EVE_START_TIME");
requiredValidator.add("EVE_OVER_TIME");
datetimeValidators[1].set("yyyy-MM-dd").add("EVE_OVER_TIME");
requiredValidator.add("EVE_DAYS");
intValidator.add("EVE_DAYS");
requiredValidator.add("EVE_REASON");
requiredValidator.add("EVE_SUBSIDY");
numValidator.add("EVE_SUBSIDY");
numValidator.add("EVE_TOTAL_MONEY");
initDetailOpertionImage();
<%}%>
$(function(){
	resetTabHeight(80);
});
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
