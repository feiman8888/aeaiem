package com.agileai.em.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jasig.cas.client.authentication.AttributePrincipal;

import com.agileai.hotweb.bizmoduler.core.SystemLogService;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.common.HttpRequestHelper;
import com.agileai.hotweb.domain.core.Profile;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.filter.HotwebUserCacher;

public class CASGeneralCasFilter implements Filter {
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, 
			ServletException {
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			AttributePrincipal attributePrincipal = (AttributePrincipal)httpRequest.getUserPrincipal();
			if(attributePrincipal != null){
		        String loginName = attributePrincipal.getName();
		        Profile profile = getProfile(httpRequest);
		        if (profile == null) {
		        	String fromIpAddress = HttpRequestHelper.getRemoteHost(httpRequest);
					String appName = httpRequest.getContextPath().substring(1);
					HotwebUserCacher userCacher = HotwebUserCacher.getInstance(appName);
					User user = userCacher.getUser(loginName);
					
					this.writeSystemLog(fromIpAddress,user, "CAS认证权限初始化", "initAuth");
					
					profile = new Profile(loginName,fromIpAddress,user);
					HttpSession session = httpRequest.getSession(true);
					session.setAttribute(Profile.PROFILE_KEY, profile);
		        }
			}
			chain.doFilter(request, response);
	}
	
	protected Profile getProfile(HttpServletRequest request){
		Profile profile = null;
		HttpSession session = request.getSession(false);
		if (session != null){
			profile = (Profile)session.getAttribute(Profile.PROFILE_KEY);	
		}
		return profile;
	}
	
	protected void writeSystemLog(String ipAddress,User user,String content, String actionType) {
		String userId = user.getUserCode();
		String userName = user.getUserName();
		SystemLogService logService = (SystemLogService) BeanFactory.instance().getBean("sysLogService");
		logService.insertLogRecord(ipAddress, userId, userName, content,actionType);
	}
	
	@Override
	public void destroy() {
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}
}
