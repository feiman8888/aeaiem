package com.agileai.em.cxmodule;

import java.util.Date;
import java.util.List;

import com.agileai.domain.DataRow;
import com.agileai.util.DateUtil;


public class BizCodeUtil {
	public static final int withPrefix = 1;
	public static final int withoutPrefix = 2;
	
	public static BizCodeUtil instance(){
		return new BizCodeUtil();
	}
	
	public static String create(String bizMark,List<DataRow> list,int type){
		String result = null;
		String strDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, new Date());
		strDate = strDate.replace("-", "");
		int tempNum = 1;
		if(list.size()!=0){
			tempNum = list.size()+1;
		}
		switch (type) {
			case 1: {
				String number = String.valueOf(Integer.valueOf(tempNum)+1000);
				number = number.substring(1);
				result = bizMark+strDate+number;
			    break;
			}
			case 2: {
				String number = String.valueOf(Integer.valueOf(tempNum)+1000);
				number = number.substring(1);
				result = strDate+number;
			    break;
			}
		}
		return result;
	}
}
