
package com.agileai.em.wsproxy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for invokeProcess complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="invokeProcess">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InvokeWorkItem" type="{http://www.agileai.com/bpm/api}invokeWorkItem" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "invokeProcess", propOrder = {
    "invokeWorkItem"
})
public class InvokeProcess {

    @XmlElement(name = "InvokeWorkItem")
    protected InvokeWorkItem invokeWorkItem;

    /**
     * Gets the value of the invokeWorkItem property.
     * 
     * @return
     *     possible object is
     *     {@link InvokeWorkItem }
     *     
     */
    public InvokeWorkItem getInvokeWorkItem() {
        return invokeWorkItem;
    }

    /**
     * Sets the value of the invokeWorkItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvokeWorkItem }
     *     
     */
    public void setInvokeWorkItem(InvokeWorkItem value) {
        this.invokeWorkItem = value;
    }

}
