
package com.agileai.em.wsproxy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for saveDraft complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="saveDraft">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="draftWorkItem" type="{http://www.agileai.com/bpm/api}draftWorkItem" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "saveDraft", propOrder = {
    "draftWorkItem"
})
public class SaveDraft {

    protected DraftWorkItem draftWorkItem;

    /**
     * Gets the value of the draftWorkItem property.
     * 
     * @return
     *     possible object is
     *     {@link DraftWorkItem }
     *     
     */
    public DraftWorkItem getDraftWorkItem() {
        return draftWorkItem;
    }

    /**
     * Sets the value of the draftWorkItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link DraftWorkItem }
     *     
     */
    public void setDraftWorkItem(DraftWorkItem value) {
        this.draftWorkItem = value;
    }

}
