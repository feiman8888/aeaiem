
package com.agileai.em.wsproxy;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for processProfile complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="processProfile">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="activeNodes" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="bizRecordId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="defineXML" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="graphJSON" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="laucheTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="laucherId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="laucherName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operaterId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operaterName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "processProfile", propOrder = {
    "activeNodes",
    "bizRecordId",
    "code",
    "defineXML",
    "graphJSON",
    "id",
    "laucheTime",
    "laucherId",
    "laucherName",
    "name",
    "operateTime",
    "operaterId",
    "operaterName",
    "status",
    "title"
})
public class ProcessProfile {

    @XmlElement(nillable = true)
    protected List<String> activeNodes;
    protected String bizRecordId;
    protected String code;
    protected String defineXML;
    protected String graphJSON;
    protected String id;
    protected String laucheTime;
    protected String laucherId;
    protected String laucherName;
    protected String name;
    protected String operateTime;
    protected String operaterId;
    protected String operaterName;
    protected String status;
    protected String title;

    /**
     * Gets the value of the activeNodes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the activeNodes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActiveNodes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getActiveNodes() {
        if (activeNodes == null) {
            activeNodes = new ArrayList<String>();
        }
        return this.activeNodes;
    }

    /**
     * Gets the value of the bizRecordId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBizRecordId() {
        return bizRecordId;
    }

    /**
     * Sets the value of the bizRecordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBizRecordId(String value) {
        this.bizRecordId = value;
    }

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the defineXML property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefineXML() {
        return defineXML;
    }

    /**
     * Sets the value of the defineXML property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefineXML(String value) {
        this.defineXML = value;
    }

    /**
     * Gets the value of the graphJSON property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGraphJSON() {
        return graphJSON;
    }

    /**
     * Sets the value of the graphJSON property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGraphJSON(String value) {
        this.graphJSON = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the laucheTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLaucheTime() {
        return laucheTime;
    }

    /**
     * Sets the value of the laucheTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLaucheTime(String value) {
        this.laucheTime = value;
    }

    /**
     * Gets the value of the laucherId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLaucherId() {
        return laucherId;
    }

    /**
     * Sets the value of the laucherId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLaucherId(String value) {
        this.laucherId = value;
    }

    /**
     * Gets the value of the laucherName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLaucherName() {
        return laucherName;
    }

    /**
     * Sets the value of the laucherName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLaucherName(String value) {
        this.laucherName = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the operateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperateTime() {
        return operateTime;
    }

    /**
     * Sets the value of the operateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperateTime(String value) {
        this.operateTime = value;
    }

    /**
     * Gets the value of the operaterId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperaterId() {
        return operaterId;
    }

    /**
     * Sets the value of the operaterId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperaterId(String value) {
        this.operaterId = value;
    }

    /**
     * Gets the value of the operaterName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperaterName() {
        return operaterName;
    }

    /**
     * Sets the value of the operaterName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperaterName(String value) {
        this.operaterName = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

}
