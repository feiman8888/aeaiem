package com.agileai.em.module.system.service;

import com.agileai.hotweb.bizmoduler.core.TreeSelectServiceImpl;
import com.agileai.em.cxmodule.SecurityGroupTreeSelect;

public class SecurityGroupTreeSelectImpl
        extends TreeSelectServiceImpl
        implements SecurityGroupTreeSelect {
    public SecurityGroupTreeSelectImpl() {
        super();
    }
}
