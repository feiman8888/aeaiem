package com.agileai.em.module.evection.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.em.module.evection.service.QueryProListSelect;
import com.agileai.hotweb.bizmoduler.core.PickFillModelServiceImpl;

public class QueryProListSelectImpl
        extends PickFillModelServiceImpl
        implements QueryProListSelect {
    public QueryProListSelectImpl() {
        super();
    }

	@Override
	public List<DataRow> queryProjectRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+queryPickFillRecordsSQL;
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
}
