package com.agileai.em.module.evection.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.PickFillModelService;

public interface QueryProListSelect
        extends PickFillModelService {

	List<DataRow> queryProjectRecords(DataParam param);
}
