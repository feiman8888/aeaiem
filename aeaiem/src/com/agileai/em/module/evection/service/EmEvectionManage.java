package com.agileai.em.module.evection.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubService;

public interface EmEvectionManage extends MasterSubService {

	void computeTotalMoney(String masterRecordId);

	List<DataRow> findApproveOpinionRecords(DataParam param);

	void createApproveRecord(DataParam queryParam);

	void changeStateRecord(DataParam newParam);

	List<DataRow> findPcNameRecords(DataParam param);

	List<DataRow> mobileFindEvectionRecords(DataParam param);

	DataRow mobileGetEvectionRecord(DataParam param);

	void createEvectionRecord(DataParam createParam);

	void updateEvectionRecord(DataParam updateParam);

	void deleteEvectionRecord(DataParam param);

	List<DataRow> findExpRecords(DataParam param);

	DataRow getExpRecord(DataParam param);

	void createExpRecord(DataParam createParam);

	void updateExpRecord(DataParam updateParam);

	void deleteExpRecord(DataParam param);

	List<DataRow> findEmPayDiagramRecords(DataParam param);

	List<DataRow> findEmPayPersonalData(String userId);

	DataRow getEmPayPersonalData(String id);

	List<DataRow> findAppList(String id);

	List<DataRow> getEmPersonalPayDiagramData(String userId);

	DataRow getTaskEveRecord(String id);

	DataRow getTaskExpreRecord(String id);

	String getUserCode(String userId);

}
