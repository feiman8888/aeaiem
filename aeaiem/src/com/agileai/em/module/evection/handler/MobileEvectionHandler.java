package com.agileai.em.module.evection.handler;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.common.AppConfig;
import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.em.common.ProcessHelper;
import com.agileai.em.module.evection.service.EmEvectionManage;
import com.agileai.em.module.evection.service.QueryProListSelect;
import com.agileai.em.wsproxy.BizAttribute;
import com.agileai.em.wsproxy.HandleWorkItem;
import com.agileai.em.wsproxy.LaunchWorkItem;
import com.agileai.em.wsproxy.ProcessService;
import com.agileai.em.wsproxy.SubmitWorkItem;
import com.agileai.em.wsproxy.WorkItem;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.common.HttpClientHelper;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class MobileEvectionHandler extends SimpleHandler{
	private static String UNSUBMITTED = "UNSUBMITTED";
	private static String AUDITING = "AUDITING";
	private static String PAID = "PAID";
	private static String CANCEL = "CANCEL";
	public static final String PROCESS_CODE = "Emevection";
	public MobileEvectionHandler(){
		super();
	}
	@PageAction
	public ViewRenderer findEvectionList(DataParam param){
		String responseText = FAIL;
		try {
			EmEvectionManage emEvectionManage = this.lookupService(EmEvectionManage.class);
			User user = (User) this.getUser();
			param.put("EVE_APPLICATION",user.getUserId());
			List<DataRow> records = emEvectionManage.mobileFindEvectionRecords(param);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<records.size();i++){
				JSONObject jsonObject1 = new JSONObject();
				DataRow row = records.get(i);
				String ID = row.getString("EVE_ID");
				String TITLE = row.getString("PC_NAME");
				String NAME = row.getString("EVE_APPLICATION_NAME");
				Date erDate = (Date) row.get("EVE_REIMBURSEMENT_TIME");
				String DATE = DateUtil.getDateByType(DateUtil.YYMMDD_SLANTING, erDate);
				String STATE = row.getString("EVE_STATE");
				String STATE_TEXT = row.getString("EVE_STATE_TEXT");
				jsonObject1.put("id", ID);
				jsonObject1.put("title", TITLE);
				jsonObject1.put("name", NAME);
				jsonObject1.put("date", DATE);
				jsonObject1.put("state", STATE);
				jsonObject1.put("stateText", STATE_TEXT);
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("eveList", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer retrieveEvectionDetail(DataParam param){
		String responseText = FAIL;
		try {
			EmEvectionManage emEvectionManage = this.lookupService(EmEvectionManage.class);
			User user = (User) this.getUser();
			param.put("EVE_APPLICATION",user.getUserId());
			String ID = param.getString("ID");
			param.put("EVE_ID",ID);
			DataRow record = emEvectionManage.mobileGetEvectionRecord(param);
			JSONObject jsonObject = new JSONObject();
			String id = record.getString("EVE_ID");
			String pcId = record.getString("PC_ID");
			String title = record.getString("PC_NAME");
			String name = record.getString("EVE_APPLICATION_NAME");
			String together = record.getString("EVE_TOGETHER");
			if(StringUtil.isNullOrEmpty(together)){
				together = "无";
			}
			Date erDate = (Date) record.get("EVE_REIMBURSEMENT_TIME");
			String date = DateUtil.getDateByType(DateUtil.YYMMDD_SLANTING, erDate);
			Date sDate = (Date) record.get("EVE_START_TIME");
			String startDate = DateUtil.getDateByType(DateUtil.YYMMDD_SLANTING, sDate);
			Date eDate = (Date) record.get("EVE_OVER_TIME");
			String overDate = DateUtil.getDateByType(DateUtil.YYMMDD_SLANTING, eDate);
			int days = record.getInt("EVE_DAYS");
			BigDecimal subsidy = (BigDecimal) record.get("EVE_SUBSIDY");
			BigDecimal money = (BigDecimal) record.get("EVE_TOTAL_MONEY");
			String remark = record.getString("EVE_REASON");
			if(StringUtil.isNullOrEmpty(remark)){
				remark = "无";
			}
			String state = record.getString("EVE_STATE");
			String stateText = record.getString("EVE_STATE_TEXT");
			jsonObject.put("id", id);
			jsonObject.put("pcId", pcId);
			jsonObject.put("title", title);
			jsonObject.put("name", name);
			jsonObject.put("together", together);
			jsonObject.put("date", date);
			jsonObject.put("startDate", startDate);
			jsonObject.put("overDate", overDate);
			jsonObject.put("days", days);
			jsonObject.put("subsidy", subsidy);
			jsonObject.put("money", money);
			jsonObject.put("desc", remark);
			jsonObject.put("state", state);
			jsonObject.put("stateText", stateText);
			List<DataRow> records = emEvectionManage.findApproveOpinionRecords(param);
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<records.size();i++){
				JSONObject jsonObject1 = new JSONObject();
				DataRow row = records.get(i);
				jsonObject1.put("appName", row.getString("EVE_APP_PERSON_NAME"));
				jsonObject1.put("appResult", row.getString("EVE_APP_RESULT_NAME"));
				jsonObject1.put("appDate", row.get("EVE_APP_TIME"));
				String appOpinion = row.getString("EVE_APP_OPINION");
				if(StringUtil.isNullOrEmpty(appOpinion)){
					appOpinion = "无";
				}
				jsonObject1.put("appOpinion",appOpinion);
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("appList", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer createEvectionRecord(DataParam param){
		String responseText = FAIL;
		try {
			EmEvectionManage emEvectionManage = this.lookupService(EmEvectionManage.class);
			User user = (User) this.getUser();
			String resquest = getInputString();
			JSONObject jsonObject = new JSONObject(resquest);			
			String pcId = jsonObject.getString("pcId");
			String strDate = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
			Date date = DateUtil.getDate(strDate);
			String together = jsonObject.getString("together");
			String startDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDate(jsonObject.getString("startDate")));
			String overDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDate(jsonObject.getString("overDate")));
			Integer days = (Integer)jsonObject.get("days");
			Integer subsidy = (Integer) jsonObject.get("subsidy");
			String remark = jsonObject.getString("desc");
			String state = UNSUBMITTED;
			String eveId = KeyGenerator.instance().genKey();
			DataParam createParam = new DataParam("EVE_ID",eveId,"PC_ID",pcId,"EVE_APPLICATION",user.getUserId(),"EVE_TOGETHER",together,"EVE_REIMBURSEMENT_TIME",date,
					"EVE_START_TIME",startDate,"EVE_OVER_TIME",overDate,"EVE_DAYS",days,"EVE_SUBSIDY",subsidy,"EVE_REASON",remark,"EVE_STATE",state);
			emEvectionManage.createEvectionRecord(createParam);
			responseText = eveId;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer editEvectionRecord(DataParam param){
		String responseText = FAIL;
		try {
			EmEvectionManage emEvectionManage = this.lookupService(EmEvectionManage.class);
			User user = (User) this.getUser();
			String id = param.getString("ID");
			String resquest = getInputString();
			JSONObject jsonObject = new JSONObject(resquest);			
			String pcId = jsonObject.getString("pcId");
			String strDate = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
			Date date = DateUtil.getDate(strDate);
			String together = jsonObject.getString("together");
			String startDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDate(jsonObject.getString("startDate")));
			String overDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDate(jsonObject.getString("overDate")));
			Integer days = (Integer)jsonObject.get("days");
			Integer money = (Integer) jsonObject.get("money");
			Integer subsidy = (Integer) jsonObject.get("subsidy");
			String remark = jsonObject.getString("desc");
			String state = UNSUBMITTED;
			DataParam updateParam = new DataParam("EVE_ID",id,"PC_ID",pcId,"EVE_APPLICATION",user.getUserId(),"EVE_TOGETHER",together,"EVE_REIMBURSEMENT_TIME",date,
					"EVE_START_TIME",startDate,"EVE_OVER_TIME",overDate,"EVE_DAYS",days,"EVE_SUBSIDY",subsidy,"EVE_TOTAL_MONEY",money,"EVE_REASON",remark,"EVE_STATE",state);
			emEvectionManage.updateEvectionRecord(updateParam);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer submitEvectionRecord(DataParam param){
		String responseText = FAIL;
		try {
			EmEvectionManage emEvectionManage = this.lookupService(EmEvectionManage.class);
			String EVE_ID = param.getString("ID");
			String EVE_STATE = AUDITING; 
			param.put("EVE_ID", EVE_ID,"EVE_STATE",EVE_STATE);
			emEvectionManage.changeStateRecord(param);
			
			DataRow row = emEvectionManage.getMasterRecord(param);
			User user = (User) this.getUser();
			ProcessHelper processHelper = ProcessHelper.instance();
			ProcessService processService = processHelper.getBPMService();
			List<BizAttribute> attributeList = new ArrayList<BizAttribute>();
			BizAttribute bizAttribute = new BizAttribute();
			bizAttribute.setCode("userId");
			bizAttribute.setValue(user.getUserId());
			attributeList.add(bizAttribute);
			String bizRecordId = EVE_ID;
			if(StringUtil.isNullOrEmpty(bizRecordId)){
				bizRecordId = KeyGenerator.instance().genKey();
			}
			String processId = processService.getCurrentProcessId(PROCESS_CODE);
			String title = row.getString("PC_NAME")+row.getString("EVE_APPLICATION_NAME");
			LaunchWorkItem launchWorkItem = processHelper.createLaunchWorkItem(processId);
			launchWorkItem.getAttributeList().addAll(attributeList);
			launchWorkItem.setTitle(title);
			launchWorkItem.setBizRecordId(bizRecordId);
			launchWorkItem.setUserCode(user.getUserCode());		
			param.put("SKIP_FIRST_NODE", "true");
			boolean skipFirstNode = "true".equals(param.get("SKIP_FIRST_NODE"));
			processService.launchProcess(launchWorkItem,skipFirstNode);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer deleteEvectionRecord(DataParam param){
		String responseText = FAIL;
		try {
			EmEvectionManage emEvectionManage = this.lookupService(EmEvectionManage.class);
			String EVE_ID = param.getString("ID");
			param.put("EVE_ID", EVE_ID);
			emEvectionManage.deleteEvectionRecord(param);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findProjectRecords(DataParam param){
		String responseText = FAIL;
		try {
			QueryProListSelect queryProListSelect = this.lookupService(QueryProListSelect.class);
			List<DataRow> records = queryProListSelect.queryProjectRecords(param);
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<records.size();i++){
				JSONObject jsonObject = new JSONObject();
				DataRow row = records.get(i); 
				jsonObject.put("pcId", row.getString("PC_ID"));
				jsonObject.put("pcName", row.getString("PC_NAME"));
				jsonArray.put(jsonObject);
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("datas",jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findEvectionExpList(DataParam param){
		String responseText = FAIL;
		try {
			EmEvectionManage emEvectionManage = this.lookupService(EmEvectionManage.class);
			param.put("EVE_ID",param.getString("ID"));
			List<DataRow> records = emEvectionManage.findExpRecords(param);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<records.size();i++){
				JSONObject jsonObject1 = new JSONObject();
				DataRow row = records.get(i);
				String ID = row.getString("EXP_ID");
				String departure = row.getString("EXP_DEPARTURE");
				String destination = row.getString("EXP_DESTINATION");
				String sdate = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING,(Date) row.get("EXP_START_TIME"));
				String edate = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING, (Date) row.get("EXP_END_TIME"));
				String transportation_way = row.getString("EXP_TRANSPORTATION_WAY");
				BigDecimal transportation_fee = (BigDecimal)row.get("EXP_TRANSPORTATION_FEE");
				BigDecimal horel_fee = (BigDecimal)row.get("EXP_HOREL_FEE");
				BigDecimal other_fee = (BigDecimal) row.get("EXP_OTHER_FEE");
				String remarks = row.getString("EXP_REMARKS");
				String state = row.getString("STATE");
				jsonObject1.put("expId", ID);
				jsonObject1.put("departure", departure);
				jsonObject1.put("destination", destination);
				jsonObject1.put("sdate", sdate);
				jsonObject1.put("edate", edate);
				jsonObject1.put("transportationWay", transportation_way);
				jsonObject1.put("transportationFee", transportation_fee);
				jsonObject1.put("horelFee", horel_fee);
				jsonObject1.put("otherFee", other_fee);
				jsonObject1.put("remarks", remarks);
				jsonObject1.put("state", state);
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("expList", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer retrieveEvectionExpList(DataParam param){
		String responseText = FAIL;
		try {
			EmEvectionManage emEvectionManage = this.lookupService(EmEvectionManage.class);
			param.put("EXP_ID",param.getString("ID"));
			DataRow row = emEvectionManage.getExpRecord(param);
			JSONObject jsonObject = new JSONObject();
			String EXP_ID = row.getString("EXP_ID");
			String EVE_ID = row.getString("EVE_ID");
			String departure = row.getString("EXP_DEPARTURE");
			String destination = row.getString("EXP_DESTINATION");
			String sdate = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING,(Date) row.get("EXP_START_TIME"));
			String edate = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING, (Date) row.get("EXP_END_TIME"));
			String transportation_way = row.getString("EXP_TRANSPORTATION_WAY");
			BigDecimal transportation_fee = (BigDecimal)row.get("EXP_TRANSPORTATION_FEE");
			BigDecimal horel_fee = (BigDecimal)row.get("EXP_HOREL_FEE");
			BigDecimal other_fee = (BigDecimal) row.get("EXP_OTHER_FEE");
			String remarks = row.getString("EXP_REMARKS");
			if(StringUtil.isNullOrEmpty(remarks)){
				remarks = "无";
			}
			String state = row.getString("state");
			jsonObject.put("expId", EXP_ID);
			jsonObject.put("eveId", EVE_ID);
			jsonObject.put("departure", departure);
			jsonObject.put("destination", destination);
			jsonObject.put("sdate", sdate);
			jsonObject.put("edate", edate);
			jsonObject.put("transportationWay", transportation_way);
			jsonObject.put("transportationFee", transportation_fee);
			jsonObject.put("horelFee", horel_fee);
			jsonObject.put("otherFee", other_fee);
			jsonObject.put("remarks", remarks);
			jsonObject.put("state", state);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer saveEvectionExpRecord(DataParam param){
		String responseText = FAIL;
		try {
			EmEvectionManage emEvectionManage = this.lookupService(EmEvectionManage.class);
			String resquest = getInputString();
			JSONObject jsonObject = new JSONObject(resquest);
			String EXP_ID = jsonObject.getString("expId");
			String EVE_ID = param.getString("eveId");
			String departure = jsonObject.getString("departure");
			String destination = jsonObject.getString("destination");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
        	String sDateUTC = jsonObject.get("sdate").toString().replace("Z", " UTC");
        	Date sDate = format.parse(sDateUTC);
        	String sdate = DateUtil.format(DateUtil.YYMMDDHHMI_HORIZONTAL, sDate);
        	String eDateUTC = jsonObject.get("edate").toString().replace("Z", " UTC");
        	Date eDate = format.parse(eDateUTC);
        	String edate = DateUtil.format(DateUtil.YYMMDDHHMI_HORIZONTAL, eDate);
        	String transportationWay = jsonObject.getString("transportationWay");
			BigDecimal transportationFee = new BigDecimal(jsonObject.getInt("transportationFee"));
			BigDecimal horelFee = new BigDecimal(jsonObject.getInt("horelFee"));
			BigDecimal otherFee = new BigDecimal(jsonObject.getInt("otherFee"));
			String remarks = jsonObject.getString("remarks");
			if(StringUtil.isNullOrEmpty(EXP_ID)){
				DataParam createParam = new DataParam("EXP_ID",null,"EVE_ID",EVE_ID,"EXP_DEPARTURE",departure,"EXP_DESTINATION",destination,"EXP_START_TIME",sdate,
						"EXP_END_TIME",edate,"EXP_TRANSPORTATION_WAY",transportationWay,"EXP_TRANSPORTATION_FEE",transportationFee,"EXP_HOREL_FEE",horelFee,"EXP_OTHER_FEE",otherFee,"EXP_REMARKS",remarks);
				emEvectionManage.createExpRecord(createParam);
			}else{
				DataParam updateParam = new DataParam("EXP_ID",EXP_ID,"EXP_DEPARTURE",departure,"EXP_DESTINATION",destination,"EXP_START_TIME",sdate,
						"EXP_END_TIME",edate,"EXP_TRANSPORTATION_WAY",transportationWay,"EXP_TRANSPORTATION_FEE",transportationFee,"EXP_HOREL_FEE",horelFee,"EXP_OTHER_FEE",otherFee,"EXP_REMARKS",remarks);
				emEvectionManage.updateExpRecord(updateParam);
			}
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer deleteExpRecord(DataParam param){
		String responseText = FAIL;
		try {
			EmEvectionManage emEvectionManage = this.lookupService(EmEvectionManage.class);
			String EXP_ID = param.getString("ID");
			param.put("EXP_ID", EXP_ID);
			emEvectionManage.deleteExpRecord(param);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}

	@PageAction
	public ViewRenderer getEmPayDiagramData(DataParam param){
		String responseText = FAIL;
		try {
			AppConfig appConfig = this.getAppConfig();
			String expendStatUrl = appConfig.getConfig("GlobalConfig", "ExpendStatUrl");
			HttpClientHelper clientHelper = new HttpClientHelper();
			String content = clientHelper.retrieveGetReponseText(expendStatUrl);
			JSONArray jsonArray = new JSONArray(content);
			JSONObject jsonObject = new JSONObject();
			JSONArray months = new JSONArray();
			JSONArray eveMoney = new JSONArray();
			JSONArray expMoney = new JSONArray();
			JSONArray salMoney = new JSONArray();
			JSONArray title = new JSONArray();
			title.put(0, "差旅费用");
			title.put(1, "普通费用");
			title.put(2, "薪资费用");
			for(int i=0;i<jsonArray.length();i++){
				JSONObject row =  (JSONObject) jsonArray.get(i);
				months.put(i, row.getString("month"));
				eveMoney.put(i, row.get("eveMoney"));
				expMoney.put(i, row.get("expMoney"));
				salMoney.put(i, row.get("salMoney"));
			}
			jsonObject.put("labels", months);
			jsonObject.put("series", title);
			JSONArray jsonArrayData = new JSONArray();
			jsonArrayData.put(0, eveMoney);
			jsonArrayData.put(1, expMoney);
			jsonArrayData.put(2, salMoney);
			jsonObject.put("data", jsonArrayData);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer getEmPayDiagramDataSheet(DataParam param){
		String responseText = FAIL;
		try {
			AppConfig appConfig = this.getAppConfig();
			String expendStatUrl = appConfig.getConfig("GlobalConfig", "ExpendStatUrl");
			HttpClientHelper clientHelper = new HttpClientHelper();
			String content = clientHelper.retrieveGetReponseText(expendStatUrl);
			JSONArray jsonArrayContent = new JSONArray(content);
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<jsonArrayContent.length();i++){
				JSONObject row = (JSONObject) jsonArrayContent.get(i);
				JSONObject jsonoObject = new JSONObject();
				jsonoObject.put("month",row.get("month"));
				jsonoObject.put("eveMoney",row.get("eveMoney"));
				jsonoObject.put("expMoney", row.get("expMoney"));
				jsonoObject.put("salMoney",row.get("salMoney"));
				BigDecimal eveMoney = new BigDecimal(row.getString("eveMoney")); 
				BigDecimal expMoney = new BigDecimal(row.getString("expMoney")); 
				BigDecimal salMoney = new BigDecimal(row.getString("salMoney")); 
				BigDecimal totalMoney = salMoney.add(eveMoney.add(expMoney));
				jsonoObject.put("totalMoney",totalMoney);
				jsonArray.put(jsonoObject);
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("emPayList", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer getEmPersonalPayDiagramData(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User) this.getUser();
			EmEvectionManage emEvectionManage = this.lookupService(EmEvectionManage.class);
			List<DataRow> records = emEvectionManage.getEmPersonalPayDiagramData(user.getUserId());
			BigDecimal noPayMoney = new BigDecimal("0.00");
			BigDecimal hasPayMoney = new BigDecimal("0.00");
			for (int i = 0; i < records.size(); i++) {
				DataRow row = records.get(i);
				BigDecimal money = (BigDecimal) row.get("money");
				if(AUDITING.equals(row.getString("state"))){
					noPayMoney = noPayMoney.add(money);
				}else if(PAID.equals(row.getString("state"))){
					hasPayMoney = hasPayMoney.add(money);
				}
			}
			JSONArray moneyArray = new JSONArray();
			moneyArray.put(0,"");
			moneyArray.put(1,noPayMoney);
			moneyArray.put(2,hasPayMoney);
			moneyArray.put(3,"");
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONArray jsonArray1 = new JSONArray();
			jsonArray.put(moneyArray);
			jsonArray1.put(0, "");
			jsonArray1.put(1, "未报销");
			jsonArray1.put(2, "已报销");
			jsonArray1.put(3, "");
			jsonObject.put("labels", jsonArray1);
			jsonObject.put("series", jsonArray1);
			jsonObject.put("data", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findEmPayPersonalData(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User) this.getUser();
			EmEvectionManage emEvectionManage = this.lookupService(EmEvectionManage.class);
			List<DataRow> records = emEvectionManage.findEmPayPersonalData(user.getUserId());
			List<DataRow> noPays = new ArrayList<DataRow>();
			List<DataRow> hasPays = new ArrayList<DataRow>();
			for(int i=0;i<records.size();i++){
				DataRow row = records.get(i);
				if(AUDITING.equals(row.getString("state"))){
					noPays.add(row);
				}
				if(PAID.equals(row.getString("state"))){
					hasPays.add(row);
				}
			}
			//未付款
			JSONArray jsonArray1 = new JSONArray();
			for(int i=0;i<noPays.size();i++){
				DataRow noPay = noPays.get(i);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("id", noPay.getString("id"));
				jsonObject.put("title", noPay.getString("title"));
				BigDecimal money = (BigDecimal) noPay.get("money");
				jsonObject.put("money", money.toString()+"元");
				jsonObject.put("date", noPay.get("date"));
				jsonArray1.put(jsonObject);
			}
			//已付款
			JSONArray jsonArray2 = new JSONArray();
			int hasPaysCount = 19;
			if(hasPays.size()<20){
				hasPaysCount = hasPays.size();
			}
			for(int i=0;i<hasPaysCount;i++){
				DataRow hasPay = hasPays.get(i);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("id", hasPay.getString("id"));
				jsonObject.put("title", hasPay.getString("title"));
				BigDecimal money = (BigDecimal) hasPay.get("money");
				jsonObject.put("money", money.toString()+"元");
				jsonObject.put("date", hasPay.get("date"));
				jsonArray2.put(jsonObject);
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("noPay", jsonArray1);
			jsonObject.put("noPayCount", noPays.size());
			jsonObject.put("hasPay", jsonArray2);
			jsonObject.put("hasPayCount", hasPays.size());
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer getEmPayPersonalData(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User) this.getUser();
			String id = param.getString("ID");
			EmEvectionManage emEvectionManage = this.lookupService(EmEvectionManage.class);
			DataRow row = emEvectionManage.getEmPayPersonalData(id);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("title", row.getString("title"));
			jsonObject.put("name", user.getUserName());
			jsonObject.put("state", row.getString("state"));
			jsonObject.put("stateText", row.getString("stateText"));
			jsonObject.put("date", row.get("date"));
			jsonObject.put("money", row.get("money")+"元");
			jsonObject.put("style", row.getString("style"));
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findTaskList(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User) this.getUser();
			AppConfig appConfig = this.getAppConfig();
			String TaskListUrl = appConfig.getConfig("GlobalConfig", "TaskListUrl");
			String dataURL= TaskListUrl+"?userCode="+user.getUserCode()+"&maxLimit=20&navigateId=03";
			HttpClientHelper clientHelper = new HttpClientHelper();
			String content = clientHelper.retrieveGetReponseText(dataURL);
			JSONArray jsonArray = new JSONArray(content);
			//待办
			JSONObject jsonObject1 = (JSONObject) jsonArray.get(0);
			JSONArray jsonArray1 = (JSONArray) jsonObject1.get("datas");
			JSONArray todoTasks = new JSONArray();
			JSONArray allTask = new JSONArray();
			for(int i =0;i<jsonArray1.length();i++){
				JSONObject jsonObject11 = (JSONObject) jsonArray1.get(i);
				String title = jsonObject11.getString("taskTitle");
				String businessId = jsonObject11.getString("businessId");
				String laucherName = jsonObject11.getString("laucherName");
				String laucheTime = jsonObject11.getString("laucheTime");
				String submiterName = jsonObject11.getString("submiterName");
				String submiteTime = jsonObject11.getString("submiteTime");
				String mobileBizUrl = jsonObject11.getString("mobileBizUrl");
				String mark = null;
				String type = null;
				if(mobileBizUrl.indexOf("expre")>-1){
					type = "expre";
					if(mobileBizUrl.indexOf("EmExpReimbursementManageAgainEdit")>-1){
						mark = "againEdit";
					}else if(mobileBizUrl.indexOf("EmExpReimbursementManageApprove")>-1){
						mark = "approve";
					}else if(mobileBizUrl.indexOf("EmExpReimbursementCashierManage")>-1){
						mark = "cashier";
					}
				}else if(mobileBizUrl.indexOf("eve")>-1){
					type = "eve";
					if(mobileBizUrl.indexOf("EmEvectionManageAgainEdit")>-1){
						mark = "againEdit";
					}else if(mobileBizUrl.indexOf("EmEvectionManageApprove")>-1){
						mark = "approve";
					}else if(mobileBizUrl.indexOf("EmEvectionCashierManage")>-1){
						mark = "cashier";
					}
				}
				JSONObject jsonObject12 = new JSONObject();
				jsonObject12.put("title", title);
				jsonObject12.put("id", businessId);
				jsonObject12.put("mark", mark);
				jsonObject12.put("type", type);
				jsonObject12.put("laucherName", laucherName);
				jsonObject12.put("laucheTime", laucheTime);
				jsonObject12.put("submiterName", submiterName);
				jsonObject12.put("submiteTime", submiteTime);
				jsonObject12.put("bizUrl", mobileBizUrl);
				todoTasks.put(jsonObject12);
				jsonObject12.put("state", "待办");
				allTask.put(jsonObject12);
			}
			//在办
			JSONObject jsonObject2 = (JSONObject) jsonArray.get(1);
			JSONArray jsonArray2 = (JSONArray) jsonObject2.get("datas");
			JSONArray ondoTasks = new JSONArray();
			ProcessHelper processHelper = ProcessHelper.instance();
			ProcessService bpmService = processHelper.getBPMService();
			for(int i =0;i<jsonArray2.length();i++){
				JSONObject jsonObject21 = (JSONObject) jsonArray2.get(i);
				String title = jsonObject21.getString("taskTitle");
				String businessId = jsonObject21.getString("businessId");
				String laucherName = null;
				String laucheTime = null;
				String activityName = null;
				WorkItem workItem = this.getActivityName(bpmService,businessId);
				JSONObject jsonObject22 = new JSONObject();
				jsonObject22.put("title", title);
				jsonObject22.put("id", businessId);
				if(workItem != null){
					laucherName = workItem.getLaucherName();
					laucheTime = workItem.getLaucheTime();
					activityName = workItem.getActivityName();
				}
				jsonObject22.put("laucherName", laucherName);
				jsonObject22.put("laucheTime", laucheTime);
				jsonObject22.put("activityName", activityName);
				ondoTasks.put(jsonObject22);
				jsonObject22.put("state", "在办");
				allTask.put(jsonObject22);
			}
			//已办
			JSONObject jsonObject3 = (JSONObject) jsonArray.get(2);
			JSONArray jsonArray3 = (JSONArray) jsonObject3.get("datas");
			JSONArray didTasks = new JSONArray();
			int count = 19;
			if(jsonArray3.length()<20){
				count = jsonArray3.length();
			}
			for(int i =0;i<count;i++){
				JSONObject jsonObject31 = (JSONObject) jsonArray3.get(i);
				String title = jsonObject31.getString("taskTitle");
				String businessId = jsonObject31.getString("businessId");
				String laucherName = jsonObject31.getString("lancherName");
				String laucheTime = jsonObject31.getString("laucheTime");
				String completerName = jsonObject31.getString("completerName");
				String completerTime = jsonObject31.getString("completerTime");
				JSONObject jsonObject32 = new JSONObject();
				jsonObject32.put("title", title);
				jsonObject32.put("id", businessId);
				jsonObject32.put("laucherName", laucherName);
				jsonObject32.put("laucheTime", laucheTime);
				jsonObject32.put("completerName", completerName);
				jsonObject32.put("completerTime", completerTime);
				didTasks.put(jsonObject32);
				jsonObject32.put("state", "已办");
				allTask.put(jsonObject32);
			}
			JSONObject taskList = new JSONObject();
			String requestFrom = param.getString("requestFrom");
			if(StringUtil.isNotNullNotEmpty(requestFrom)){
				taskList.put("allTaskList", allTask);
			}else{
				taskList.put("todoTasks", todoTasks);
				taskList.put("ondoTasks", ondoTasks);
				taskList.put("didTasks", didTasks);
				taskList.put("todoTaskCount", todoTasks.length());
				taskList.put("ondoTaskCount", ondoTasks.length());
				taskList.put("didTaskCount", didTasks.length());
			}
			responseText = taskList.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer approveTask(DataParam param){
		String responseText = FAIL;
		try {
			String resquest = getInputString();
			JSONObject jsonObject = new JSONObject(resquest);
			User user = (User) getUser();
			DataParam queryParam = new DataParam();
			queryParam.put("EVE_APP_ID",KeyGenerator.instance().genKey());
			queryParam.put("EVE_ID",jsonObject.get("id"));
			queryParam.put("EVE_APP_PERSON",user.getUserId());
			queryParam.put("EVE_APP_OPINION",jsonObject.get("opinionContent"));
			queryParam.put("EVE_APP_RESULT",jsonObject.get("opinion"));
			queryParam.put("EVE_APP_TIME",new Date());
			EmEvectionManage mobileManage = this.lookupService(EmEvectionManage.class);
			mobileManage.createApproveRecord(queryParam);
			
			ProcessHelper processHelper = ProcessHelper.instance();
			BizAttribute bizAttribute = new BizAttribute();
			bizAttribute.setCode("AppoperType");
			bizAttribute.setValue(jsonObject.getString("opinion"));
			BizAttribute bizAttribute1 = new BizAttribute();
			bizAttribute1.setCode("totalMoney");
			String money = String.valueOf(jsonObject.get("money"));
			bizAttribute1.setValue(money);
			List<BizAttribute> attributeList = new ArrayList<BizAttribute>();
			attributeList.add(bizAttribute);
			attributeList.add(bizAttribute1);
			ProcessService bpmService = ProcessHelper.instance().getBPMService();
			String mobileBizUrl = (String) jsonObject.get("bizUrl");
			String[] flowInfoArray = mobileBizUrl.split("/");
			String processId = flowInfoArray[2];
			String processInstId = flowInfoArray[3];
			String activityCode = flowInfoArray[4];
			SubmitWorkItem processWorkItem = processHelper.createSubmitWorkItem(processId, processInstId, activityCode);
			if(StringUtil.isNullOrEmpty(attributeList.get(0).getCode())){
				attributeList = processWorkItem.getAttributeList();
			}
			processWorkItem.setUserCode(user.getUserCode());
			processWorkItem.getAttributeList().addAll(attributeList);
			bpmService.submitProcess(processWorkItem);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer submitTask(DataParam param){
		String responseText = FAIL;
		try {
			String resquest = getInputString();
			JSONObject jsonObject = new JSONObject(resquest);
			User user = (User) getUser();
			EmEvectionManage mobileManage = this.lookupService(EmEvectionManage.class);
			String id = param.getString("ID");
			String pcId = jsonObject.getString("pcId");
			String strDate = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
			Date date = DateUtil.getDate(strDate);
			String together = jsonObject.getString("together");
			String startDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDate(jsonObject.getString("startDate")));
			String overDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDate(jsonObject.getString("overDate")));
			Integer days = (Integer)jsonObject.get("days");
			Integer money = (Integer) jsonObject.get("money");
			Integer subsidy = (Integer) jsonObject.get("subsidy");
			String remark = jsonObject.getString("desc");
			String state = AUDITING;
			DataParam updateParam = new DataParam("EVE_ID",id,"PC_ID",pcId,"EVE_APPLICATION",user.getUserId(),"EVE_TOGETHER",together,"EVE_REIMBURSEMENT_TIME",date,
					"EVE_START_TIME",startDate,"EVE_OVER_TIME",overDate,"EVE_DAYS",days,"EVE_SUBSIDY",subsidy,"EVE_TOTAL_MONEY",money,"EVE_REASON",remark,"EVE_STATE",state);
			mobileManage.updateEvectionRecord(updateParam);
			
			List<BizAttribute> attributeList = new ArrayList<BizAttribute>();
			String mobileBizUrl = (String) param.get("bizUrl");
			String[] flowInfoArray = mobileBizUrl.split("/");
			String processId = flowInfoArray[2];
			String processInstId = flowInfoArray[3];
			String activityCode = flowInfoArray[4];
			
			ProcessHelper processHelper = ProcessHelper.instance();
			ProcessService bpmService = processHelper.getBPMService();
			SubmitWorkItem processWorkItem = processHelper.createSubmitWorkItem(processId, processInstId, activityCode);
			attributeList = processWorkItem.getAttributeList();
			processWorkItem.setUserCode(user.getUserCode());
			processWorkItem.getAttributeList().addAll(attributeList);
			bpmService.submitProcess(processWorkItem);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	@PageAction
	public ViewRenderer cancelTask(DataParam param){
		String responseText = FAIL;
		try {
			String resquest = getInputString();
			JSONObject jsonObject = new JSONObject(resquest);
			User user = (User) getUser();
			EmEvectionManage mobileManage = this.lookupService(EmEvectionManage.class);
			String id = param.getString("ID");
			String pcId = jsonObject.getString("pcId");
			String strDate = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
			Date date = DateUtil.getDate(strDate);
			String together = jsonObject.getString("together");
			String startDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDate(jsonObject.getString("startDate")));
			String overDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDate(jsonObject.getString("overDate")));
			Integer days = (Integer)jsonObject.get("days");
			Integer money = (Integer) jsonObject.get("money");
			Integer subsidy = (Integer) jsonObject.get("subsidy");
			String remark = jsonObject.getString("desc");
			String state = CANCEL;
			DataParam updateParam = new DataParam("EVE_ID",id,"PC_ID",pcId,"EVE_APPLICATION",user.getUserId(),"EVE_TOGETHER",together,"EVE_REIMBURSEMENT_TIME",date,
					"EVE_START_TIME",startDate,"EVE_OVER_TIME",overDate,"EVE_DAYS",days,"EVE_SUBSIDY",subsidy,"EVE_TOTAL_MONEY",money,"EVE_REASON",remark,"EVE_STATE",state);
			mobileManage.updateEvectionRecord(updateParam);

			ProcessHelper processHelper = ProcessHelper.instance();
			ProcessService processService = processHelper.getBPMService();
			String mobileBizUrl = param.getString("bizUrl");
			String[] flowInfoArray = mobileBizUrl.split("/");
			String processId = flowInfoArray[2];
			String processInstId = flowInfoArray[3];
			
			HandleWorkItem handleWorkItem = new HandleWorkItem();
			handleWorkItem.setProcessId(processId);
			handleWorkItem.setProcessInstanceId(processInstId);
			handleWorkItem.setUserCode(user.getUserCode());
			processService.terminateProcess(handleWorkItem);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer cashTask(DataParam param){
		String responseText = FAIL;
		try {
			String resquest = getInputString();
			JSONObject jsonObject = new JSONObject(resquest);	
			User user = (User) getUser();
			DataParam newParam = new DataParam();
			newParam.put("EVE_ID",jsonObject.get("id"));
			newParam.put("EVE_STATE", PAID);
			EmEvectionManage mobileManage = this.lookupService(EmEvectionManage.class);
			mobileManage.changeStateRecord(newParam);
			List<BizAttribute> attributeList = new ArrayList<BizAttribute>();
			BizAttribute bizAttribute = new BizAttribute();
	   		bizAttribute.setCode("userId");
	   		bizAttribute.setValue(user.getUserId());
	   		attributeList.add(bizAttribute);
	   		
	   		ProcessHelper processHelper = ProcessHelper.instance();
			ProcessService bpmService = processHelper.getBPMService();
			String mobileBizUrl = (String) jsonObject.get("bizUrl");
			String[] flowInfoArray = mobileBizUrl.split("/");
			String processId = flowInfoArray[2];
			String processInstId = flowInfoArray[3];
			String activityCode = flowInfoArray[4];
			SubmitWorkItem processWorkItem = processHelper.createSubmitWorkItem(processId, processInstId, activityCode);
			if(StringUtil.isNullOrEmpty(attributeList.get(0).getCode())){
				attributeList = processWorkItem.getAttributeList();
			}
			processWorkItem.setUserCode(user.getUserCode());
			processWorkItem.getAttributeList().addAll(attributeList);
			bpmService.submitProcess(processWorkItem);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	private WorkItem getActivityName(ProcessService bpmService,String id){
		WorkItem workItem = null;
		List<WorkItem> taskItemList = bpmService.findWorkItems("");
		for (int i = 0; i < taskItemList.size(); i++) {
			WorkItem taskItem = taskItemList.get(i);
			String bizId = taskItem.getBusinessId();
			if(bizId.equals(id)){
				workItem =  taskItem;
			}
		}
		return workItem;
	}
	
	
	@PageAction
	public ViewRenderer findOnDoingAppList(DataParam param){
		String responseText = FAIL;
		try {
			String id = param.get("ID");
			ProcessHelper processHelper = ProcessHelper.instance();
			ProcessService bpmService = processHelper.getBPMService();
			WorkItem workItem = this.getActivityName(bpmService,id);
			String result = this.getEmInfoById(param);
			JSONObject jsonObject = new JSONObject(result);
			jsonObject.put("activityName", workItem.getActivityName());
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findDidAppList(DataParam param){
		String responseText = FAIL;
		try {
			String result = this.getEmInfoById(param);
			responseText = result;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	private String getEmInfoById(DataParam param){
		String result = null;
		try {
			String id = param.get("ID");
			EmEvectionManage emEvectionManage = this.lookupService(EmEvectionManage.class);
			List<DataRow> approveList = emEvectionManage.findAppList(id);
			DataRow record = emEvectionManage.getTaskEveRecord(id);
			JSONObject jsonObject = new JSONObject();
			JSONObject tempJson = new JSONObject();
			if(record!=null){
				String eveId = record.getString("EVE_ID");
				String title = record.getString("PC_NAME");
				String name = record.getString("EVE_APPLICATION_NAME");
				String together = record.getString("EVE_TOGETHER");
				if(StringUtil.isNullOrEmpty(together)){
					together = "无";
				}
				Date erDate = (Date) record.get("EVE_REIMBURSEMENT_TIME");
				String date = DateUtil.getDateByType(DateUtil.YYMMDD_SLANTING, erDate);
				Date sDate = (Date) record.get("EVE_START_TIME");
				String startDate = DateUtil.getDateByType(DateUtil.YYMMDD_SLANTING, sDate);
				Date eDate = (Date) record.get("EVE_OVER_TIME");
				String overDate = DateUtil.getDateByType(DateUtil.YYMMDD_SLANTING, eDate);
				int days = record.getInt("EVE_DAYS");
				BigDecimal subsidy = (BigDecimal) record.get("EVE_SUBSIDY");
				BigDecimal money = (BigDecimal) record.get("EVE_TOTAL_MONEY");
				String remark = record.getString("EVE_REASON");
				if(StringUtil.isNullOrEmpty(remark)){
					remark = "无";
				}
				String stateText = record.getString("EVE_STATE_TEXT");
				tempJson.put("eveId", eveId);
				tempJson.put("title", title);
				tempJson.put("name", name);
				tempJson.put("together", together);
				tempJson.put("date", date);
				tempJson.put("startDate", startDate);
				tempJson.put("overDate", overDate);
				tempJson.put("days", days);
				tempJson.put("subsidy", subsidy);
				tempJson.put("money", money);
				tempJson.put("desc", remark);
				tempJson.put("stateText", stateText);
				//清单
				param.put("EVE_ID",param.getString("ID"));
				List<DataRow> records = emEvectionManage.findExpRecords(param);
				JSONArray jsonArray = new JSONArray();
				for(int i=0;i<records.size();i++){
					JSONObject jsonObject1 = new JSONObject();
					DataRow row = records.get(i);
					String ID = row.getString("EXP_ID");
					String departure = row.getString("EXP_DEPARTURE");
					String destination = row.getString("EXP_DESTINATION");
					String sdate = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING,(Date) row.get("EXP_START_TIME"));
					String edate = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING, (Date) row.get("EXP_END_TIME"));
					String transportation_way = row.getString("EXP_TRANSPORTATION_WAY_TEXT");
					BigDecimal transportation_fee = (BigDecimal)row.get("EXP_TRANSPORTATION_FEE");
					BigDecimal horel_fee = (BigDecimal)row.get("EXP_HOREL_FEE");
					BigDecimal other_fee = (BigDecimal) row.get("EXP_OTHER_FEE");
					String remarks = row.getString("EXP_REMARKS");
					if(StringUtil.isNullOrEmpty(remarks)){
						remarks = "无";
					}
					String state = row.getString("STATE");
					jsonObject1.put("expId", ID);
					jsonObject1.put("departure", departure);
					jsonObject1.put("destination", destination);
					jsonObject1.put("sdate", sdate);
					jsonObject1.put("edate", edate);
					jsonObject1.put("transportationWay", transportation_way);
					jsonObject1.put("transportationFee", transportation_fee);
					jsonObject1.put("horelFee", horel_fee);
					jsonObject1.put("otherFee", other_fee);
					jsonObject1.put("remarks", remarks);
					jsonObject1.put("state", state);
					jsonArray.put(jsonObject1);
				}
				tempJson.put("expList", jsonArray);
				jsonObject.put("taskFrom","eve");
			}else{
				record = emEvectionManage.getTaskExpreRecord(id);
				String erId = record.getString("ER_ID");
				String title = record.getString("ER_TITLE");
				String name = record.getString("ER_PERSON_NAME");
				Date erDate = (Date) record.get("ER_DATE");
				String date = DateUtil.getDateByType(DateUtil.YYMMDD_SLANTING, erDate);
				BigDecimal money = (BigDecimal) record.get("ER_MONEY");
				String style = record.getString("ER_STYLE_TEXT");
				String remark = record.getString("ER_REMARKS");
				String state = record.getString("ER_STATE");
				String stateText = record.getString("ER_STATE_TEXT");
				tempJson.put("id", erId);
				tempJson.put("title", title);
				tempJson.put("name", name);
				tempJson.put("date", date);
				tempJson.put("money", money);
				tempJson.put("style", style);
				tempJson.put("desc", remark);
				tempJson.put("state", state);
				tempJson.put("stateText", stateText);
				jsonObject.put("taskFrom","expre");
			}
			jsonObject.put("content", tempJson);
			JSONArray jsonArray = new JSONArray();
			for (int i = 0; i < approveList.size(); i++) {
				DataRow approveRow = approveList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("id", approveRow.getString("APP_ID"));
				jsonObject1.put("approver", approveRow.getString("APPROVER"));
				jsonObject1.put("appDateTime", approveRow.get("APP_DATETIME"));
				jsonObject1.put("appResult", approveRow.getString("APP_RESULT"));
				jsonObject1.put("appResultCode", approveRow.getString("APP_RESULT_CODE"));
				String appOpinion = approveRow.getString("APP_OPINION");
				if(StringUtil.isNullOrEmpty(appOpinion)){
					appOpinion = "无";
				}
				jsonObject1.put("appOpinion", appOpinion);
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("appList", jsonArray);
			result = jsonObject.toString();
		}catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return result;
	}
	}
