package com.agileai.em.module.evection.handler;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.em.common.ProcessHelper;
import com.agileai.em.module.evection.service.EmEvectionManage;
import com.agileai.em.wsproxy.BizAttribute;
import com.agileai.em.wsproxy.ProcessService;
import com.agileai.em.wsproxy.SubmitWorkItem;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.bizmoduler.core.MasterSubService;
import com.agileai.hotweb.controller.core.MasterSubEditMainHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.StringUtil;

public class EmEvectionManageApproveHandler
        extends MasterSubEditMainHandler {
    public EmEvectionManageApproveHandler() {
        super();
        this.listHandlerClass = EmEvectionManageListHandler.class;
        this.serviceId = buildServiceId(EmEvectionManage.class);
        this.baseTablePK = "EVE_ID";
        this.defaultTabId = "_base";
    }
    public ViewRenderer prepareDisplay(DataParam param) {
		param.put("EVE_ID",param.get("WFIP_BUSINESS_ID"));
		DataRow record = getService().getMasterRecord(param);
		this.setAttributes(record);			
		List<DataRow> records = getService().findApproveOpinionRecords(param);
		setAttribute("ApproceOpinionRecords", records);
		String currentSubTableId = param.get("currentSubTableId",defaultTabId);
		String mark = param.getString("mark");
		if(StringUtil.isNotNullNotEmpty(mark)&&!"null".equals(mark)){
			this.setAttribute("onlyShowInfo", true);
		}else {
			this.setAttribute("onlyShowInfo", false);
		}
		if (!currentSubTableId.equals(MasterSubService.BASE_TABLE_ID)){
			String subRecordsKey = currentSubTableId+"Records";
			if (!this.getAttributesContainer().containsKey(subRecordsKey)){
				List<DataRow> subRecords = getService().findSubRecords(currentSubTableId, param);
				this.setAttribute(currentSubTableId+"Records", subRecords);
			}
		}
		this.setAttribute("currentSubTableId", currentSubTableId);
		this.setAttribute("currentSubTableIndex", getTabIndex(currentSubTableId));
		String operateType = param.get(OperaType.KEY);
		this.setOperaType(operateType);
		setAttributes(param);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    
    protected void processPageAttributes(DataParam param) {
        setAttribute("EVE_STATE",FormSelectFactory.create("EM_STATE")
        		.addSelectedValue(getOperaAttributeValue("EVE_STATE","UNSUBMITTED")));
        setAttribute("EVE_APP_RESULT", FormSelectFactory.create("APPOPERTYPE")
				.addSelectedValue(getAttributeValue("EVE_APP_RESULT", "Y")));
        initMappingItem("EVP_TRANSPORTATION_WAY",
                FormSelectFactory.create("TRANSPORTATION_WAY").getContent());
    }
    
    protected String[] getEntryEditFields(String currentSubTableId) {
        List<String> temp = new ArrayList<String>();
        return temp.toArray(new String[] {  });
    }
    
    @PageAction
    public ViewRenderer submit(DataParam param) {
		String responseText = FAIL;
		try {
			User user = (User) getUser();
			DataParam queryParam = new DataParam();
			queryParam.put("EVE_APP_ID",KeyGenerator.instance().genKey());
			queryParam.put("EVE_ID",param.get("EVE_ID"));
			queryParam.put("EVE_APP_PERSON",user.getUserId());
			queryParam.put("EVE_APP_OPINION",param.get("EVE_APP_OPINION"));
			queryParam.put("EVE_APP_RESULT",param.get("EVE_APP_RESULT"));
			queryParam.put("EVE_APP_TIME",new Date());
			getService().createApproveRecord(queryParam);
			
			ProcessHelper processHelper = ProcessHelper.instance();
			BizAttribute bizAttribute = new BizAttribute();
			bizAttribute.setCode("AppoperType");
			bizAttribute.setValue(param.getString("EVE_APP_RESULT"));
			BizAttribute bizAttribute1 = new BizAttribute();
			bizAttribute1.setCode("totalMoney");
			bizAttribute1.setValue(param.get("EVE_TOTAL_MONEY"));
			List<BizAttribute> attributeList = new ArrayList<BizAttribute>();
			attributeList.add(bizAttribute);
			attributeList.add(bizAttribute1);
			
			ProcessService bpmService = ProcessHelper.instance().getBPMService();
			String processId = param.getString("WFP_ID");
			String processInstId = param.getString("WFIP_ID");
			String activityCode = param.getString("WFA_CODE");
			String title = param.get("PC_NAME")+"_"+param.get("EVE_APPLICATION_NAME");
			SubmitWorkItem processWorkItem = processHelper.createSubmitWorkItem(processId, processInstId, activityCode);
			if(StringUtil.isNullOrEmpty(attributeList.get(0).getCode())){
				attributeList = processWorkItem.getAttributeList();
			}
			processWorkItem.setUserCode(user.getUserCode());
			processWorkItem.setTitle(title);
			processWorkItem.getAttributeList().addAll(attributeList);
			bpmService.submitProcess(processWorkItem);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		responseText = SUCCESS;
		return new AjaxRenderer(responseText);
	}
    
    protected String getEntryEditTablePK(String currentSubTableId) {
        HashMap<String, String> primaryKeys = new HashMap<String, String>();
        primaryKeys.put("EmExpenses", "EXP_ID");
        return primaryKeys.get(currentSubTableId);
    }
    
    protected String getEntryEditForeignKey(String currentSubTableId) {
        HashMap<String, String> foreignKeys = new HashMap<String, String>();
        foreignKeys.put("EmExpenses", "EVE_ID");
        return foreignKeys.get(currentSubTableId);
    }
    
    protected EmEvectionManage getService() {
        return (EmEvectionManage) this.lookupService(this.getServiceId());
    }
}
