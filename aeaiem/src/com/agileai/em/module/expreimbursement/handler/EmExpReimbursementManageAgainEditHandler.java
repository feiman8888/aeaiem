package com.agileai.em.module.expreimbursement.handler;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.em.common.ProcessHelper;
import com.agileai.em.module.expreimbursement.service.EmExpReimbursementManage;
import com.agileai.em.wsproxy.BizAttribute;
import com.agileai.em.wsproxy.HandleWorkItem;
import com.agileai.em.wsproxy.ProcessService;
import com.agileai.em.wsproxy.SubmitWorkItem;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class EmExpReimbursementManageAgainEditHandler
        extends StandardEditHandler {
    public EmExpReimbursementManageAgainEditHandler() {
        super();
        this.listHandlerClass = EmExpReimbursementManageListHandler.class;
        this.serviceId = buildServiceId(EmExpReimbursementManage.class);
    }
    public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		param.put("ER_ID",param.get("WFIP_BUSINESS_ID"));
		DataRow record = getService().getRecord(param);
		this.setAttributes(record);	
		List<DataRow> records = getService().findApproveOpinionRecords(param);
		setAttribute("ApproceOpinionRecords", records);
		setAttributes(param);
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    
    @PageAction
    public ViewRenderer submit(DataParam param) {
    	String responseText = FAIL;
    	try {
    		User user = (User) getUser();
    		param.put("ER_STATE", "AUDITING");
    		getService().updateRecord(param);
    		
    		List<BizAttribute> attributeList = new ArrayList<BizAttribute>();
    		String processId = param.getString("WFP_ID");
    		String processInstId = param.getString("WFIP_ID");
    		String activityCode = param.getString("WFA_CODE");
    		
    		ProcessHelper processHelper = ProcessHelper.instance();
    		ProcessService bpmService = processHelper.getBPMService();
    		SubmitWorkItem processWorkItem = processHelper.createSubmitWorkItem(processId, processInstId, activityCode);
    		attributeList = processWorkItem.getAttributeList();
    		processWorkItem.setUserCode(user.getUserCode());
    		processWorkItem.getAttributeList().addAll(attributeList);
    		bpmService.submitProcess(processWorkItem);
    		responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
	}
    
    @PageAction
    public ViewRenderer cancel(DataParam param)throws Exception {
		String responseText = FAIL;
		try {
			User user = (User) getUser();
			param.put("ER_STATE", "CANCEL");
			getService().updateRecord(param);
			ProcessHelper processHelper = ProcessHelper.instance();
	   		ProcessService processService = processHelper.getBPMService();
			String processId = param.getString("WFP_ID");
			String processInstId = param.getString("WFIP_ID");
			
			HandleWorkItem handleWorkItem = new HandleWorkItem();
			handleWorkItem.setProcessId(processId);
			handleWorkItem.setProcessInstanceId(processInstId);
			handleWorkItem.setUserCode(user.getUserCode());
			processService.terminateProcess(handleWorkItem);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
    
    protected void processPageAttributes(DataParam param) {
        setAttribute("ER_STYLE",FormSelectFactory.create("ER_STYLE")
        		.addSelectedValue(getAttributeValue("ER_STYLE","")));
        setAttribute("ER_STATE",FormSelectFactory.create("EM_STATE")
        		.addSelectedValue(getAttributeValue("ER_STATE","UNSUBMITTED")));
    }

    protected EmExpReimbursementManage getService() {
        return (EmExpReimbursementManage) this.lookupService(this.getServiceId());
    }
}
