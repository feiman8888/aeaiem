package com.agileai.em.module.expreimbursement.handler;

import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.common.AppConfig;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.em.common.PrivilegeHelper;
import com.agileai.em.common.ProcessHelper;
import com.agileai.em.module.expreimbursement.service.EmExpReimbursementManage;
import com.agileai.em.wsproxy.ProcessService;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class EmExpReimbursementManageListHandler
        extends StandardListHandler {
	public static final String PROCESS_CODE = "Expreimbursement";
    public EmExpReimbursementManageListHandler() {
        super();
        this.editHandlerClazz = EmExpReimbursementManageEditHandler.class;
        this.serviceId = buildServiceId(EmExpReimbursementManage.class);
    }
    public ViewRenderer prepareDisplay(DataParam param){
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		User user = (User) this.getUser();
		PrivilegeHelper privilegeHelper = new PrivilegeHelper(user);
		if(privilegeHelper.isFinance()||privilegeHelper.isManager()){
			param.put("CANSHOW", "UNSUBMITTED");
			param.put("SHOWPERSON", user.getUserId());
			this.setAttribute("showCancelBtn", true);
		}else{
			param.put("ER_PERSON", user.getUserId());
			this.setAttribute("showCancelBtn", false);
		}
		List<DataRow> rsList = getService().findRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    
    protected void processPageAttributes(DataParam param) {
        setAttribute("erStyle",FormSelectFactory.create("ER_STYLE")
        		.addSelectedValue(param.get("erStyle")));
        setAttribute("erState",FormSelectFactory.create("EM_STATE")
        		.addSelectedValue(param.get("erState")));
        initMappingItem("ER_STYLE",FormSelectFactory.create("ER_STYLE").getContent());
        initMappingItem("ER_STATE",FormSelectFactory.create("EM_STATE").getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "erStyle", "");
        initParamItem(param, "pcName", "");
        initParamItem(param, "erState", "");
        initParamItem(param,"sdate",DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(DateUtil.getBeginOfMonth(new Date()),DateUtil.MONTH,-1)));
		initParamItem(param,"edate",DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, 1)));
    }
    
    @PageAction
    public ViewRenderer cancel(DataParam param)throws Exception {
		String responseText = FAIL;
		try {
			param.put("ER_STATE", "CANCEL");
			getService().changeStateRecord(param);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
    
    @PageAction
	public ViewRenderer retrieveIds(DataParam param) throws JSONException{
		String responseText = "";
		AppConfig appConfig = BeanFactory.instance().getAppConfig();
   		String BpmShowFlowUrl = appConfig.getConfig("GlobalConfig", "BpmShowFlowUrl");
		ProcessHelper processHelper = ProcessHelper.instance();
		ProcessService bpmService = processHelper.getBPMService();
		String bizRecordId = param.getString("ER_ID");
		List<String> aa = bpmService.getProcessId8InstId(PROCESS_CODE, bizRecordId);
		JSONObject jsonObject = new JSONObject();  
        jsonObject.put("processId", aa.get(0));
        jsonObject.put("processInstId", aa.get(1));
        jsonObject.put("BpmShowFlowUrl", BpmShowFlowUrl);
        String json = jsonObject.toString();
		responseText=json;
		return new AjaxRenderer(responseText);
	}
    protected EmExpReimbursementManage getService() {
        return (EmExpReimbursementManage) this.lookupService(this.getServiceId());
    }
}
