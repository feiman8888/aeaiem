package com.agileai.em.module.bpm.handler;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.common.AppConfig;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.em.common.ProcessHelper;
import com.agileai.em.wsproxy.ProcessService;
import com.agileai.em.wsproxy.WorkItem;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.controller.core.QueryModelListHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class TaskListQueryHandler extends QueryModelListHandler{
	public TaskListQueryHandler(){
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param){
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = new ArrayList<DataRow>();
		User user = (User)this.getUser();
		String userCode = user.getUserCode();

		ProcessService bpmService = ProcessHelper.instance().getBPMService();
		List<WorkItem> taskItemList = bpmService.findWorkItems(userCode);
		
		processRecords(taskItemList, rsList);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	
	public ViewRenderer doRedirectAction(DataParam param){
		storeParam(param);
		String url = param.get("bizFormURL");
		url = url+"&comeFromTaskList=true";
		return new RedirectRenderer(url);
	}
	
	private void processRecords(List<WorkItem> taskItemList,List<DataRow> rsList){
		if (taskItemList != null){
			for (int i=0;i < taskItemList.size();i++){
				WorkItem taskItem = taskItemList.get(i);
				DataRow row = new DataRow();
				row.put("WFIP_ID",taskItem.getProcInstanceId());
				row.put("WFP_ID",taskItem.getProcessId());
				row.put("WFA_CODE",taskItem.getActivityCode());
				row.put("WFIP_BUSINESS_ID",taskItem.getBusinessId());
				row.put("WFP_NAME",taskItem.getProcessName());
				row.put("WFA_NAME",taskItem.getActivityName());
				row.put("WFIP_TITLE",taskItem.getTitle());
				row.put("WFIP_BIZURL",taskItem.getBusinessURL());
				row.put("WFIP_TITLE",taskItem.getTitle());
				row.put("WFIP_LAUCHER_NAME",taskItem.getLaucherName());
				row.put("WFIP_LAUCHER_TIME",DateUtil.getDateTime(taskItem.getLaucheTime()));
				row.put("WFIP_OPERATER_NAME",taskItem.getSubmiterName());
				row.put("WFIP_OPERATER_TIME",DateUtil.getDateTime(taskItem.getSubmiteTime()));
				rsList.add(row);
			}			
		}
	}
	@PageAction
  	public ViewRenderer retrieveIds(DataParam param) throws JSONException{
  		String responseText = "";
  		AppConfig appConfig = BeanFactory.instance().getAppConfig();
 		String BpmShowFlowUrl = appConfig.getConfig("GlobalConfig", "BpmShowFlowUrl");
 		setAttribute("BpmShowFlowUrl", BpmShowFlowUrl);
  		JSONObject jsonObject = new JSONObject();  
          jsonObject.put("processId", param.get("processId"));
          jsonObject.put("processInstId", param.get("processInstId"));
          jsonObject.put("BpmShowFlowUrl", BpmShowFlowUrl);
          String json = jsonObject.toString();
  		responseText=json;
  		return new AjaxRenderer(responseText);
  	}
	protected void processPageAttributes(DataParam param) {
		
	}
	
	protected void initParameters(DataParam param) {
		
	}
}	
