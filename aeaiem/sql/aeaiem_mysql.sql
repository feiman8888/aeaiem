/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50151
Source Host           : localhost:3306
Source Database       : emtest

Target Server Type    : MYSQL
Target Server Version : 50151
File Encoding         : 65001

Date: 2015-12-17 09:44:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for em_evection
-- ----------------------------
DROP TABLE IF EXISTS `em_evection`;
CREATE TABLE `em_evection` (
  `EVE_ID` char(36) NOT NULL,
  `PC_ID` char(36) DEFAULT NULL,
  `EVE_APPLICATION` char(36) DEFAULT NULL,
  `EVE_TOGETHER` varchar(128) DEFAULT NULL,
  `EVE_START_TIME` date DEFAULT NULL,
  `EVE_OVER_TIME` date DEFAULT NULL,
  `EVE_REIMBURSEMENT_TIME` date DEFAULT NULL,
  `EVE_DAYS` int(3) DEFAULT NULL,
  `EVE_REASON` varchar(512) DEFAULT NULL,
  `EVE_SUBSIDY` decimal(8,2) DEFAULT NULL,
  `EVE_TOTAL_MONEY` decimal(8,2) DEFAULT NULL,
  `EVE_STATE` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`EVE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of em_evection
-- ----------------------------

-- ----------------------------
-- Table structure for em_eve_app_opinion
-- ----------------------------
DROP TABLE IF EXISTS `em_eve_app_opinion`;
CREATE TABLE `em_eve_app_opinion` (
  `EVE_APP_ID` char(36) NOT NULL,
  `EVE_ID` char(36) DEFAULT NULL,
  `EVE_APP_PERSON` char(36) DEFAULT NULL,
  `EVE_APP_TIME` timestamp NULL DEFAULT NULL,
  `EVE_APP_OPINION` varchar(256) DEFAULT NULL,
  `EVE_APP_RESULT` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`EVE_APP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of em_eve_app_opinion
-- ----------------------------

-- ----------------------------
-- Table structure for em_expenses
-- ----------------------------
DROP TABLE IF EXISTS `em_expenses`;
CREATE TABLE `em_expenses` (
  `EXP_ID` char(36) NOT NULL,
  `EVE_ID` char(36) DEFAULT NULL,
  `EXP_DEPARTURE` varchar(128) DEFAULT NULL,
  `EXP_DESTINATION` varchar(128) DEFAULT NULL,
  `EXP_START_TIME` timestamp NULL DEFAULT NULL,
  `EXP_END_TIME` timestamp NULL DEFAULT NULL,
  `EXP_TRANSPORTATION_WAY` varchar(32) DEFAULT NULL,
  `EXP_TRANSPORTATION_FEE` decimal(8,2) DEFAULT NULL,
  `EXP_HOREL_FEE` decimal(8,2) DEFAULT NULL,
  `EXP_OTHER_FEE` decimal(8,2) DEFAULT NULL,
  `EXP_REMARKS` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`EXP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of em_expenses
-- ----------------------------

-- ----------------------------
-- Table structure for em_exp_app_opinion
-- ----------------------------
DROP TABLE IF EXISTS `em_exp_app_opinion`;
CREATE TABLE `em_exp_app_opinion` (
  `EXP_APP_ID` char(36) NOT NULL,
  `ER_ID` char(36) DEFAULT NULL,
  `EXP_APP_PERSON` char(36) DEFAULT NULL,
  `EXP_APP_RESULT` varchar(32) DEFAULT NULL,
  `EXP_APP_TIME` timestamp NULL DEFAULT NULL,
  `EXP_APP_OPINION` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`EXP_APP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of em_exp_app_opinion
-- ----------------------------

-- ----------------------------
-- Table structure for em_exp_reimbursement
-- ----------------------------
DROP TABLE IF EXISTS `em_exp_reimbursement`;
CREATE TABLE `em_exp_reimbursement` (
  `ER_ID` char(36) NOT NULL,
  `PC_ID` char(36) DEFAULT NULL,
  `ER_TITLE` varchar(128) DEFAULT NULL,
  `ER_PERSON` char(36) DEFAULT NULL,
  `ER_DATE` date DEFAULT NULL,
  `ER_MONEY` decimal(8,2) DEFAULT NULL,
  `ER_STYLE` varchar(32) DEFAULT NULL,
  `ER_ILLUSTRATE` varchar(512) DEFAULT NULL,
  `ER_REMARKS` varchar(512) DEFAULT NULL,
  `ER_STATE` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of em_exp_reimbursement
-- ----------------------------

-- ----------------------------
-- Table structure for em_pro_config
-- ----------------------------
DROP TABLE IF EXISTS `em_pro_config`;
CREATE TABLE `em_pro_config` (
  `PC_ID` char(36) NOT NULL,
  `PC_CODE` varchar(32) DEFAULT NULL,
  `PC_NAME` varchar(32) DEFAULT NULL,
  `PC_STYLE` varchar(32) DEFAULT NULL,
  `PC_LOCATION` varchar(128) DEFAULT NULL,
  `PC_FIRST_PARTY` varchar(32) DEFAULT NULL,
  `PC_FIRST_PARTY_HEAD` varchar(32) DEFAULT NULL,
  `PC_RESPONSIBLE_PERSON` varchar(32) DEFAULT NULL,
  `PC_SALES_PERSON` varchar(32) DEFAULT NULL,
  `PC_MONEY` decimal(10,2) DEFAULT NULL,
  `PC_REMARKS` varchar(512) DEFAULT NULL,
  `PC_START_DATE` date DEFAULT NULL,
  `PC_END_DATE` date DEFAULT NULL,
  `PC_STATE` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`PC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of em_pro_config
-- ----------------------------
INSERT INTO `em_pro_config` VALUES ('4CC22A1F-6B5C-433D-8A85-8F597EE79DCA', 'SOAINTEGR-20151216-01', '测试项目', 'SOAINTEGR', '沈阳', '数通畅联', '', '', 'cs02', '0.00', '', null, null, 'SUBMIT');

-- ----------------------------
-- Table structure for security_group
-- ----------------------------
DROP TABLE IF EXISTS `security_group`;
CREATE TABLE `security_group` (
  `GRP_ID` varchar(36) NOT NULL,
  `GRP_CODE` varchar(32) DEFAULT NULL,
  `GRP_NAME` varchar(32) DEFAULT NULL,
  `GRP_PID` varchar(36) DEFAULT NULL,
  `GRP_DESC` varchar(128) DEFAULT NULL,
  `GRP_STATE` varchar(1) DEFAULT NULL,
  `GRP_SORT` int(11) DEFAULT NULL,
  PRIMARY KEY (`GRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_group
-- ----------------------------
INSERT INTO `security_group` VALUES ('00000000-0000-0000-00000000000000000', 'Root', '公司集团', null, null, '1', '0');
INSERT INTO `security_group` VALUES ('44723B8E-EDD8-4571-87F1-6A99927E24C6', 'Tech', '技术部', '00000000-0000-0000-00000000000000000', '', '1', '3');
INSERT INTO `security_group` VALUES ('66465842-2219-4FEA-A2E0-CFABF0E19073', 'Market', '营销部', '00000000-0000-0000-00000000000000000', '', '1', '5');
INSERT INTO `security_group` VALUES ('68ABC008-0547-4CC3-B074-152EFF3812FA', 'HR', '人力部', '00000000-0000-0000-00000000000000000', '', '1', '4');

-- ----------------------------
-- Table structure for security_group_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_group_auth`;
CREATE TABLE `security_group_auth` (
  `GRP_AUTH_ID` varchar(36) NOT NULL,
  `GRP_ID` varchar(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`GRP_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_group_auth
-- ----------------------------

-- ----------------------------
-- Table structure for security_role
-- ----------------------------
DROP TABLE IF EXISTS `security_role`;
CREATE TABLE `security_role` (
  `ROLE_ID` varchar(36) NOT NULL,
  `ROLE_CODE` varchar(32) DEFAULT NULL,
  `ROLE_NAME` varchar(32) DEFAULT NULL,
  `ROLE_PID` varchar(36) DEFAULT NULL,
  `ROLE_DESC` varchar(128) DEFAULT NULL,
  `ROLE_STATE` varchar(32) DEFAULT NULL,
  `ROLE_SORT` int(11) DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_role
-- ----------------------------
INSERT INTO `security_role` VALUES ('00000000-0000-0000-00000000000000000', 'System', '系统角色', null, null, '1', null);
INSERT INTO `security_role` VALUES ('299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'Clerk', '普通员工', '00000000-0000-0000-00000000000000000', '', '1', '4');
INSERT INTO `security_role` VALUES ('2A355C2A-59B2-4DDC-9425-A1DC237660D4', 'Manager', '经理', '00000000-0000-0000-00000000000000000', '', '1', '5');
INSERT INTO `security_role` VALUES ('B805EEFE-C067-4B54-95CE-4C50AF5692D9', 'Cashier', '出纳', '00000000-0000-0000-00000000000000000', '', '1', '7');
INSERT INTO `security_role` VALUES ('DBF04F17-7DD2-4072-90BA-198E724A672F', 'Finance', '财务', '00000000-0000-0000-00000000000000000', '', '1', '6');

-- ----------------------------
-- Table structure for security_role_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_role_auth`;
CREATE TABLE `security_role_auth` (
  `ROLE_AUTH_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ROLE_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_role_auth
-- ----------------------------
INSERT INTO `security_role_auth` VALUES ('0139A6D4-7FF8-4188-A61F-4E7207D67EDE', 'EE72F936-5261-43E2-AD8F-06B5A3B67E1E', 'Menu', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477');
INSERT INTO `security_role_auth` VALUES ('07C30BD0-8642-4E6A-A5EE-B932D5DAF524', '1CDFEC8A-B493-4903-A456-0E78924B21E4', 'Menu', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477');
INSERT INTO `security_role_auth` VALUES ('084835DC-A31D-44C7-8877-0A004B84C91C', '13D5B3A2-A31F-4205-B410-E868FF2C5C40', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('0F8FB325-7EAB-4989-AB0A-C80420DBD0B2', '13D5B3A2-A31F-4205-B410-E868FF2C5C40', 'Menu', 'F2971ADA-620C-4BE2-B198-90D085632E57');
INSERT INTO `security_role_auth` VALUES ('14C26973-E3E2-4821-B2E0-A9C5892AAA77', '06D78E6C-26C3-43D5-911D-8C2E40ECC57F', 'Handler', 'E6F32D4A-51C5-440F-8062-CB743C0D5480');
INSERT INTO `security_role_auth` VALUES ('154EBADF-7E1B-4E70-BD32-B3DD1949BB54', '13D5B3A2-A31F-4205-B410-E868FF2C5C40', 'Menu', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477');
INSERT INTO `security_role_auth` VALUES ('17BDF204-82F0-47A4-9813-389AB50E51F9', '1CDFEC8A-B493-4903-A456-0E78924B21E4', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO `security_role_auth` VALUES ('196F4FF1-EFC1-4E7F-8A71-F87F8E3041FC', 'EE72F936-5261-43E2-AD8F-06B5A3B67E1E', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('221A0D3E-E48E-4AD5-AC7F-9095927D0942', '1CDFEC8A-B493-4903-A456-0E78924B21E4', 'Menu', 'F2971ADA-620C-4BE2-B198-90D085632E57');
INSERT INTO `security_role_auth` VALUES ('2838395A-9E6E-4A54-8D27-297F7321B4BF', 'F34B19D3-3863-4028-AC43-8A42B2269E01', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO `security_role_auth` VALUES ('355B5C2B-8715-421D-8504-A5C690CBDDD1', 'F34B19D3-3863-4028-AC43-8A42B2269E01', 'Menu', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `security_role_auth` VALUES ('35AFEC9C-331E-40D0-8D48-F5F3866B6E2D', '13D5B3A2-A31F-4205-B410-E868FF2C5C40', 'Menu', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `security_role_auth` VALUES ('377168C5-24B7-4433-89F4-EBDC64421EB5', '13D5B3A2-A31F-4205-B410-E868FF2C5C40', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO `security_role_auth` VALUES ('38734BB6-BD8D-454C-8C31-2CF40FD0D79F', '06D78E6C-26C3-43D5-911D-8C2E40ECC57F', 'Menu', '1159A65E-E809-46AD-8B44-6F927A4B0385');
INSERT INTO `security_role_auth` VALUES ('3E223552-3511-4FC1-8081-4E44F8B791D3', 'FD0FB178-236D-42F7-8AB2-62C530F4767A', 'Menu', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477');
INSERT INTO `security_role_auth` VALUES ('49FF2C0A-84B5-42FA-A00F-E898FE42D96B', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'Menu', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477');
INSERT INTO `security_role_auth` VALUES ('507B6A62-7B10-445B-AFC6-4B5C2A6AF880', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'Menu', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `security_role_auth` VALUES ('54D20D0C-E833-4336-9B08-9D6A4C4855C8', '4FF46519-EAA1-41D6-B8F1-24B29F6F73AA', 'Menu', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477');
INSERT INTO `security_role_auth` VALUES ('60C7408D-25E8-42E3-80F6-6755A1B56168', '1CDFEC8A-B493-4903-A456-0E78924B21E4', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('626646A1-5585-4E4C-9954-63A0E249B07F', '13D5B3A2-A31F-4205-B410-E868FF2C5C40', 'Menu', '761295E9-AEA2-4D48-A781-9E750EA46789');
INSERT INTO `security_role_auth` VALUES ('6DE4B24D-F26A-4858-AAFE-663144D32C96', 'F34B19D3-3863-4028-AC43-8A42B2269E01', 'Menu', 'E269D481-2AD1-4C00-AE00-58CD687A8681');
INSERT INTO `security_role_auth` VALUES ('71AC214E-4B9E-41BE-9368-B44087FBABE1', 'FD0FB178-236D-42F7-8AB2-62C530F4767A', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('78718744-8B28-4C28-BA98-A2F694AE0C56', '1CDFEC8A-B493-4903-A456-0E78924B21E4', 'Menu', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `security_role_auth` VALUES ('8002CFCD-CCF5-4D3C-85B2-150CB213C086', '06D78E6C-26C3-43D5-911D-8C2E40ECC57F', 'Menu', 'E269D481-2AD1-4C00-AE00-58CD687A8681');
INSERT INTO `security_role_auth` VALUES ('857AAC48-16F0-4845-9BED-D33F1F16D3E9', 'F34B19D3-3863-4028-AC43-8A42B2269E01', 'Menu', 'F2971ADA-620C-4BE2-B198-90D085632E57');
INSERT INTO `security_role_auth` VALUES ('8681377C-48E0-43C6-85D8-F660921EC08B', 'F34B19D3-3863-4028-AC43-8A42B2269E01', 'Menu', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477');
INSERT INTO `security_role_auth` VALUES ('877FD410-FD2B-44AA-A3CE-D9B4B45F8269', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'Menu', '761295E9-AEA2-4D48-A781-9E750EA46789');
INSERT INTO `security_role_auth` VALUES ('88484081-AC84-4B07-B4B7-44C799FBDB03', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO `security_role_auth` VALUES ('8D3648AB-656A-4D4C-AFD2-89B2537F2E39', 'F34B19D3-3863-4028-AC43-8A42B2269E01', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('93A1FB50-8914-4CD4-B27B-045E7F54D33B', '2A355C2A-59B2-4DDC-9425-A1DC237660D4', 'Menu', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477');
INSERT INTO `security_role_auth` VALUES ('9C30F07B-4EA2-44D7-9F65-92A2E547A0DA', '13D5B3A2-A31F-4205-B410-E868FF2C5C40', 'Menu', 'E269D481-2AD1-4C00-AE00-58CD687A8681');
INSERT INTO `security_role_auth` VALUES ('9FE7664E-B4DC-46FC-B5E8-3AB4865DC42C', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'Menu', 'F2971ADA-620C-4BE2-B198-90D085632E57');
INSERT INTO `security_role_auth` VALUES ('A193F3C0-7206-4B92-A0F0-E47278A2661D', '06D78E6C-26C3-43D5-911D-8C2E40ECC57F', 'Menu', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477');
INSERT INTO `security_role_auth` VALUES ('A7205543-8EE5-4160-B536-36690A69BD05', '06D78E6C-26C3-43D5-911D-8C2E40ECC57F', 'Menu', '761295E9-AEA2-4D48-A781-9E750EA46789');
INSERT INTO `security_role_auth` VALUES ('AF039AE2-64B4-4E7D-9F2B-F9248DCCAC9E', '06D78E6C-26C3-43D5-911D-8C2E40ECC57F', 'Menu', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `security_role_auth` VALUES ('BB49EBC0-95F7-4BEB-8A1F-1C47CF6FA13D', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('BE4F23CA-1F00-469B-ABA7-4B4B2DB7928D', 'F34B19D3-3863-4028-AC43-8A42B2269E01', 'Menu', '761295E9-AEA2-4D48-A781-9E750EA46789');
INSERT INTO `security_role_auth` VALUES ('C008419F-2029-43F6-AF86-45BF6863D7D4', '2A355C2A-59B2-4DDC-9425-A1DC237660D4', 'Menu', '1159A65E-E809-46AD-8B44-6F927A4B0385');
INSERT INTO `security_role_auth` VALUES ('C3F961A3-EA9E-48E0-B52D-7C23A7721D84', '1CDFEC8A-B493-4903-A456-0E78924B21E4', 'Menu', '761295E9-AEA2-4D48-A781-9E750EA46789');
INSERT INTO `security_role_auth` VALUES ('C984BAB0-F9BA-4371-A4B8-292B4A83E26A', '1CDFEC8A-B493-4903-A456-0E78924B21E4', 'Menu', 'E269D481-2AD1-4C00-AE00-58CD687A8681');
INSERT INTO `security_role_auth` VALUES ('D34A473C-0F79-463D-A7C6-BCC672EAE81F', '2A355C2A-59B2-4DDC-9425-A1DC237660D4', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('D4B8CC38-14DD-4C5C-A542-9A131A4C34BB', '06D78E6C-26C3-43D5-911D-8C2E40ECC57F', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO `security_role_auth` VALUES ('DFCBDD63-B29A-45DE-893E-33CAD3FF0F36', '1CDFEC8A-B493-4903-A456-0E78924B21E4', 'Handler', '081B8F8A-7BAD-434B-A34D-4404A1E34D21');
INSERT INTO `security_role_auth` VALUES ('E14D2ABF-528C-40EF-B5A9-E105D028822C', '4FF46519-EAA1-41D6-B8F1-24B29F6F73AA', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('E35746A8-E583-4A7B-973C-71DED5E3AE45', '299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'Menu', 'E269D481-2AD1-4C00-AE00-58CD687A8681');
INSERT INTO `security_role_auth` VALUES ('F40FD23F-BB56-41E9-BAC8-A4D8A2CE40C2', '06D78E6C-26C3-43D5-911D-8C2E40ECC57F', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('F98BDCFD-A58A-484A-B95C-A3F4C2B903CD', '06D78E6C-26C3-43D5-911D-8C2E40ECC57F', 'Menu', 'F2971ADA-620C-4BE2-B198-90D085632E57');

-- ----------------------------
-- Table structure for security_role_group_rel
-- ----------------------------
DROP TABLE IF EXISTS `security_role_group_rel`;
CREATE TABLE `security_role_group_rel` (
  `GRP_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`GRP_ID`,`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_role_group_rel
-- ----------------------------
INSERT INTO `security_role_group_rel` VALUES ('00000000-0000-0000-00000000000000000', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_group_rel` VALUES ('21E50836-912E-4DB8-82CB-9B32C8A44C9F', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_group_rel` VALUES ('21E50836-912E-4DB8-82CB-9B32C8A44C9F', '25788B54-CE0A-4137-8890-EFA4F0DE06B6');
INSERT INTO `security_role_group_rel` VALUES ('23F5915B-C8F1-4BA5-AED9-7CE50B11D5F4', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_group_rel` VALUES ('23F5915B-C8F1-4BA5-AED9-7CE50B11D5F4', '25788B54-CE0A-4137-8890-EFA4F0DE06B6');
INSERT INTO `security_role_group_rel` VALUES ('315F898C-A008-4F77-BAB0-4FDF935F7B1F', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_group_rel` VALUES ('315F898C-A008-4F77-BAB0-4FDF935F7B1F', '25788B54-CE0A-4137-8890-EFA4F0DE06B6');
INSERT INTO `security_role_group_rel` VALUES ('BBD420A2-68AE-49C2-B3D8-78DC166F511F', '00000000-0000-0000-00000000000000000');

-- ----------------------------
-- Table structure for security_user
-- ----------------------------
DROP TABLE IF EXISTS `security_user`;
CREATE TABLE `security_user` (
  `USER_ID` varchar(36) NOT NULL,
  `USER_CODE` varchar(32) DEFAULT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `USER_PWD` varchar(32) DEFAULT NULL,
  `USER_SEX` varchar(1) DEFAULT NULL,
  `USER_DESC` varchar(128) DEFAULT NULL,
  `USER_STATE` varchar(32) DEFAULT NULL,
  `USER_SORT` int(11) DEFAULT NULL,
  `USER_MAIL` varchar(64) DEFAULT NULL,
  `USER_PHONE` varchar(64) DEFAULT NULL,
  `DISPLAY_COUNT` int(11) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user
-- ----------------------------
INSERT INTO `security_user` VALUES ('5BCB9801-7C45-4F11-9E71-634D7913A145', 'CS05', '翟小五', 'DD9F799386E27C0D8CC93556AB819203', 'M', '', '1', '5', '', '', '20');
INSERT INTO `security_user` VALUES ('6565BFC1-E424-455F-88EB-AD602D7A19C2', 'CS01', '张老大', 'DAE6CDC2B49F7C32164BE2AF1A7916AA', 'M', '', '1', '1', '', '', '20');
INSERT INTO `security_user` VALUES ('7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'admin', '管理员', '21232F297A57A5A743894A0E4A801FC3', 'M', '内置账户，勿删！！', '1', '0', '', '', '5');
INSERT INTO `security_user` VALUES ('8102F8E4-BDA0-42AE-9A45-2A08DC8C5F7A', 'CS03', '孙小三', '25EC07853BDC7B87DD021F5580C70855', 'M', '', '1', '3', '', '', '20');
INSERT INTO `security_user` VALUES ('9B84A740-64D6-4CAA-A4F2-D3B285B8969B', 'CS02', '赵小二', '39661AF3C6AFE19E95700A0E7373446A', 'M', '', '1', '2', '', '', '20');
INSERT INTO `security_user` VALUES ('B4EA9168-CB1E-4069-BAA9-39DE3FECA45D', 'CS04', '赵小四', 'FBAF749E6A085DCBBE41FF5C030EBF98', 'M', '', '1', '4', '', '', '20');

-- ----------------------------
-- Table structure for security_user_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_user_auth`;
CREATE TABLE `security_user_auth` (
  `USER_AUTH_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`USER_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user_auth
-- ----------------------------

-- ----------------------------
-- Table structure for security_user_group_rel
-- ----------------------------
DROP TABLE IF EXISTS `security_user_group_rel`;
CREATE TABLE `security_user_group_rel` (
  `GRP_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`GRP_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user_group_rel
-- ----------------------------
INSERT INTO `security_user_group_rel` VALUES ('00000000-0000-0000-00000000000000000', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24');
INSERT INTO `security_user_group_rel` VALUES ('44723B8E-EDD8-4571-87F1-6A99927E24C6', '5BCB9801-7C45-4F11-9E71-634D7913A145');
INSERT INTO `security_user_group_rel` VALUES ('44723B8E-EDD8-4571-87F1-6A99927E24C6', '6565BFC1-E424-455F-88EB-AD602D7A19C2');
INSERT INTO `security_user_group_rel` VALUES ('44723B8E-EDD8-4571-87F1-6A99927E24C6', '709F595E-BB40-4397-B5D6-AA0BB3359F1F');
INSERT INTO `security_user_group_rel` VALUES ('44723B8E-EDD8-4571-87F1-6A99927E24C6', '8102F8E4-BDA0-42AE-9A45-2A08DC8C5F7A');
INSERT INTO `security_user_group_rel` VALUES ('44723B8E-EDD8-4571-87F1-6A99927E24C6', 'B4EA9168-CB1E-4069-BAA9-39DE3FECA45D');
INSERT INTO `security_user_group_rel` VALUES ('66465842-2219-4FEA-A2E0-CFABF0E19073', '9B84A740-64D6-4CAA-A4F2-D3B285B8969B');

-- ----------------------------
-- Table structure for security_user_role_rel
-- ----------------------------
DROP TABLE IF EXISTS `security_user_role_rel`;
CREATE TABLE `security_user_role_rel` (
  `ROLE_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`ROLE_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user_role_rel
-- ----------------------------
INSERT INTO `security_user_role_rel` VALUES ('299BFEBC-8CF0-4D8F-A4A0-08520F14D257', '5BCB9801-7C45-4F11-9E71-634D7913A145');
INSERT INTO `security_user_role_rel` VALUES ('299BFEBC-8CF0-4D8F-A4A0-08520F14D257', '6565BFC1-E424-455F-88EB-AD602D7A19C2');
INSERT INTO `security_user_role_rel` VALUES ('299BFEBC-8CF0-4D8F-A4A0-08520F14D257', '709F595E-BB40-4397-B5D6-AA0BB3359F1F');
INSERT INTO `security_user_role_rel` VALUES ('299BFEBC-8CF0-4D8F-A4A0-08520F14D257', '8102F8E4-BDA0-42AE-9A45-2A08DC8C5F7A');
INSERT INTO `security_user_role_rel` VALUES ('299BFEBC-8CF0-4D8F-A4A0-08520F14D257', '9B84A740-64D6-4CAA-A4F2-D3B285B8969B');
INSERT INTO `security_user_role_rel` VALUES ('299BFEBC-8CF0-4D8F-A4A0-08520F14D257', 'B4EA9168-CB1E-4069-BAA9-39DE3FECA45D');
INSERT INTO `security_user_role_rel` VALUES ('2A355C2A-59B2-4DDC-9425-A1DC237660D4', '6565BFC1-E424-455F-88EB-AD602D7A19C2');
INSERT INTO `security_user_role_rel` VALUES ('B805EEFE-C067-4B54-95CE-4C50AF5692D9', '9B84A740-64D6-4CAA-A4F2-D3B285B8969B');
INSERT INTO `security_user_role_rel` VALUES ('DBF04F17-7DD2-4072-90BA-198E724A672F', '709F595E-BB40-4397-B5D6-AA0BB3359F1F');
INSERT INTO `security_user_role_rel` VALUES ('DBF04F17-7DD2-4072-90BA-198E724A672F', '9B84A740-64D6-4CAA-A4F2-D3B285B8969B');

-- ----------------------------
-- Table structure for sys_codelist
-- ----------------------------
DROP TABLE IF EXISTS `sys_codelist`;
CREATE TABLE `sys_codelist` (
  `TYPE_ID` varchar(32) NOT NULL,
  `CODE_ID` varchar(64) NOT NULL,
  `CODE_NAME` varchar(64) DEFAULT NULL,
  `CODE_DESC` varchar(128) DEFAULT NULL,
  `CODE_SORT` int(11) DEFAULT NULL,
  `CODE_FLAG` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`TYPE_ID`,`CODE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_codelist
-- ----------------------------
INSERT INTO `sys_codelist` VALUES ('APPOPERTYPE', 'N', '不同意', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('APPOPERTYPE', 'Y', '同意', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CODE_TYPE_GROUP', 'app_code_define', '应用编码', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CODE_TYPE_GROUP', 'sys_code_define', '系统编码', '系统编码123a1', '3', '1');
INSERT INTO `sys_codelist` VALUES ('EM_STATE', 'AUDITING', '审核中', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('EM_STATE', 'CANCEL', '已废置', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('EM_STATE', 'PAID', '已付款', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('EM_STATE', 'UNSUBMITTED', '未提交', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('ER_STYLE', 'DAILYCOST', '日常费用', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('ER_STYLE', 'OVERTIMECOST', '加班费用', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('ER_STYLE', 'TEEMBUILDINGCOST', '团队建设费用', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('FUNCTION_TYPE', 'funcmenu', '功能菜单', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('FUNCTION_TYPE', 'funcnode', '功能节点', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('HANDLER_TYPE', 'MAIN', '主控制器', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('HANDLER_TYPE', 'OTHER', '其他控制器', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('MENUTREE_CASCADE', '0', '关闭', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('MENUTREE_CASCADE', '1', '展开', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('OPER_CTR_TYPE', 'disableMode', '不能操作', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('OPER_CTR_TYPE', 'hiddenMode', '隐藏按钮', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('PC_STATE', 'INIT', '未提交', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('PC_STATE', 'SUBMIT', '已提交', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('PC_STYLE', 'ESBINTEGR', 'ESB集成', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('PC_STYLE', 'EXTDP', '扩展开发', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('PC_STYLE', 'OTHER', '其他', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('PC_STYLE', 'PORTALINTEGR', '门户集成', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('PC_STYLE', 'SOAINTEGR', 'SOA集成', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('POSITION_TYPE', 'dummy_postion', '虚拟岗位', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('POSITION_TYPE', 'real_postion', '实际岗位', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('RES_TYPE', 'IMAGE', '图片文件', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('RES_TYPE', 'ISO', '镜像文件', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('RES_TYPE', 'VIDEO', '视频文件', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('SYS_VALID_TYPE', '0', '无效', 'null', '2', '1');
INSERT INTO `sys_codelist` VALUES ('SYS_VALID_TYPE', '1', '有效', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('TRANSPORTATION_WAY', 'CAR', '汽车', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('TRANSPORTATION_WAY', 'OTHER', '其他方式', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('TRANSPORTATION_WAY', 'PLANE', '飞机', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('TRANSPORTATION_WAY', 'TRAIN', '火车', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('UNIT_TYPE', 'dept', '部门', '', '10', '1');
INSERT INTO `sys_codelist` VALUES ('UNIT_TYPE', 'org', '机构', '', '20', '1');
INSERT INTO `sys_codelist` VALUES ('UNIT_TYPE', 'post', '岗位', '', '30', '1');
INSERT INTO `sys_codelist` VALUES ('USER_SEX', 'F', '女', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('USER_SEX', 'M', '男', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Bottom', 'BottomHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Building', 'BuildingHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Homepage', 'HomepageHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Logo', 'LogoHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'MainWin', 'MainWinHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'MenuTree', 'MenuTreeHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Navigater', 'NavigaterHandler', '', '1', '1');

-- ----------------------------
-- Table structure for sys_codetype
-- ----------------------------
DROP TABLE IF EXISTS `sys_codetype`;
CREATE TABLE `sys_codetype` (
  `TYPE_ID` varchar(32) NOT NULL,
  `TYPE_NAME` varchar(32) DEFAULT NULL,
  `TYPE_GROUP` varchar(32) DEFAULT NULL,
  `TYPE_DESC` varchar(128) DEFAULT NULL,
  `IS_CACHED` char(1) DEFAULT NULL,
  `IS_UNITEADMIN` char(1) DEFAULT NULL,
  `IS_EDITABLE` char(1) DEFAULT NULL,
  `LEGNTT_LIMIT` varchar(6) DEFAULT NULL,
  `CHARACTER_LIMIT` char(1) DEFAULT NULL,
  `EXTEND_SQL` char(1) DEFAULT NULL,
  `SQL_BODY` varchar(512) DEFAULT NULL,
  `SQL_COND` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_codetype
-- ----------------------------
INSERT INTO `sys_codetype` VALUES ('APPOPERTYPE', '审批结果类型', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('BOOL_DEFINE', '布尔定义', 'sys_code_define', '', 'Y', 'Y', 'Y', '1', 'C', '', '', '');
INSERT INTO `sys_codetype` VALUES ('CODE_TYPE_GROUP', '编码类型分组', 'app_code_define', '编码类型分组', null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('EM_STATE', '费用报销状态', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('ER_STYLE', '报销类型', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('FUNCTION_TYPE', '功能类型', 'sys_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('HANDLER_TYPE', '控制器类型', 'sys_code_define', '', 'N', 'Y', 'Y', '32', 'C', null, null, null);
INSERT INTO `sys_codetype` VALUES ('MENUTREE_CASCADE', '是否展开', 'sys_code_define', '', 'Y', 'Y', 'Y', '1', 'N', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('OPER_CTR_TYPE', '操作控制类型', 'sys_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('PC_STATE', '项目状态', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('PC_STYLE', '项目类型', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('POSITION_TYPE', '岗位类型', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('RES_TYPE', '资源类型', 'sys_code_define', '', 'N', 'Y', 'Y', '16', 'C', null, null, null);
INSERT INTO `sys_codetype` VALUES ('SYS_VALID_TYPE', '有效标识符', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('TRANSPORTATION_WAY', '交通方式', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('UNIT_TYPE', '单位类型', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('USER_SEX', '性别类型', 'sys_code_define', '', 'N', 'Y', 'Y', '16', 'C', null, null, null);
INSERT INTO `sys_codetype` VALUES ('AuthedHandlerId', '认证Handler定义', 'sys_code_define', '', 'Y', 'Y', 'Y', '', 'B', 'N', '', '');

-- ----------------------------
-- Table structure for sys_function
-- ----------------------------
DROP TABLE IF EXISTS `sys_function`;
CREATE TABLE `sys_function` (
  `FUNC_ID` varchar(36) NOT NULL,
  `FUNC_NAME` varchar(64) DEFAULT NULL,
  `FUNC_TYPE` varchar(32) DEFAULT NULL,
  `MAIN_HANDLER` varchar(36) DEFAULT NULL,
  `FUNC_PID` varchar(36) DEFAULT NULL,
  `FUNC_STATE` char(1) DEFAULT NULL,
  `FUNC_SORT` int(11) DEFAULT NULL,
  `FUNC_DESC` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`FUNC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_function
-- ----------------------------
INSERT INTO `sys_function` VALUES ('00000000-0000-0000-00000000000000000', '费用管理系统', 'funcmenu', null, null, '1', null, null);
INSERT INTO `sys_function` VALUES ('00000000-0000-0000-00000000000000001', '系统管理', 'funcmenu', '', '00000000-0000-0000-00000000000000000', '1', '100', '');
INSERT INTO `sys_function` VALUES ('1159A65E-E809-46AD-8B44-6F927A4B0385', '项目配置', 'funcnode', 'E6F32D4A-51C5-440F-8062-CB743C0D5480', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '1', '');
INSERT INTO `sys_function` VALUES ('5FDEE3AB-6D32-4C5F-AD65-EF1EE5FFBAE6', '附件管理', 'funcnode', '1F617665-FC8B-4E8C-ABE4-540C363A17A8', '00000000-0000-0000-00000000000000001', '1', '7', '');
INSERT INTO `sys_function` VALUES ('67BA273A-DD31-48D0-B78C-1D60D5316074', '系统日志', 'funcnode', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', '00000000-0000-0000-00000000000000001', '1', '6', null);
INSERT INTO `sys_function` VALUES ('692B0D37-2E66-4E82-92B4-E59BCF76EE76', '编码管理', 'funcnode', 'B4FE5722-9EA6-47D8-8770-D999A3F6A354', '00000000-0000-0000-00000000000000001', '1', '5', null);
INSERT INTO `sys_function` VALUES ('761295E9-AEA2-4D48-A781-9E750EA46789', '任务列表', 'funcnode', '1FAC2635-16DE-40DD-980D-75C8E61293C1', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '4', '');
INSERT INTO `sys_function` VALUES ('8C84B439-2788-4608-89C4-8F5AA076D124', '组织机构', 'funcnode', '439949F0-C6B7-49FF-8ED1-2A1B5062E7B9', '00000000-0000-0000-00000000000000001', '1', '1', null);
INSERT INTO `sys_function` VALUES ('A0334956-426E-4E49-831B-EB00E37285FD', '编码类型', 'funcnode', '9A16D554-F989-438A-B92D-C8C8AC6BF9B8', '00000000-0000-0000-00000000000000001', '1', '4', null);
INSERT INTO `sys_function` VALUES ('AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '报销管理', 'funcmenu', '', '00000000-0000-0000-00000000000000000', '1', '99', '');
INSERT INTO `sys_function` VALUES ('C977BC31-C78F-4B16-B0C6-769783E46A06', '功能管理', 'funcnode', '46C52D33-8797-4251-951F-F7CA23C76BD7', '00000000-0000-0000-00000000000000001', '1', '3', null);
INSERT INTO `sys_function` VALUES ('D3582A2A-3173-4F92-B1AD-2F999A2CBE18', '修改密码', 'funcnode', '88882DB9-967F-430E-BA9C-D0BBBBD2BD0C', '00000000-0000-0000-00000000000000001', '1', '8', '');
INSERT INTO `sys_function` VALUES ('DFE8BE4C-4024-4A7B-8DF2-630003832AE9', '角色管理', 'funcnode', '0CE03AD4-FF29-4FDB-8FEE-DA8AA38B649F', '00000000-0000-0000-00000000000000001', '1', '2', null);
INSERT INTO `sys_function` VALUES ('E269D481-2AD1-4C00-AE00-58CD687A8681', '出差报销', 'funcnode', '01260378-E704-4FC0-8BA7-81536B0C117D', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '3', '');
INSERT INTO `sys_function` VALUES ('F2971ADA-620C-4BE2-B198-90D085632E57', '费用报销', 'funcnode', 'FD79EC7E-B544-4D2D-B571-D66A583B0812', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '2', '');

-- ----------------------------
-- Table structure for sys_handler
-- ----------------------------
DROP TABLE IF EXISTS `sys_handler`;
CREATE TABLE `sys_handler` (
  `HANLER_ID` varchar(36) NOT NULL,
  `HANLER_CODE` varchar(64) DEFAULT NULL,
  `HANLER_TYPE` varchar(32) DEFAULT NULL,
  `HANLER_URL` varchar(128) DEFAULT NULL,
  `FUNC_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`HANLER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_handler
-- ----------------------------
INSERT INTO `sys_handler` VALUES ('01260378-E704-4FC0-8BA7-81536B0C117D', 'EmEvectionManageList', 'MAIN', null, 'E269D481-2AD1-4C00-AE00-58CD687A8681');
INSERT INTO `sys_handler` VALUES ('081B8F8A-7BAD-434B-A34D-4404A1E34D21', 'EmExpReimbursementManageEdit', 'OTHER', '', 'F2971ADA-620C-4BE2-B198-90D085632E57');
INSERT INTO `sys_handler` VALUES ('0CE03AD4-FF29-4FDB-8FEE-DA8AA38B649F', 'SecurityRoleTreeManage', 'MAIN', '', 'DFE8BE4C-4024-4A7B-8DF2-630003832AE9');
INSERT INTO `sys_handler` VALUES ('1F617665-FC8B-4E8C-ABE4-540C363A17A8', 'WcmGeneralGroup8ContentList', 'MAIN', null, '5FDEE3AB-6D32-4C5F-AD65-EF1EE5FFBAE6');
INSERT INTO `sys_handler` VALUES ('1FAC2635-16DE-40DD-980D-75C8E61293C1', 'TaskListQuery', 'MAIN', null, '761295E9-AEA2-4D48-A781-9E750EA46789');
INSERT INTO `sys_handler` VALUES ('3666B5D3-C140-41B1-BCD2-C968E6627553', 'EmProConfigManageEdit', 'OTHER', '', '1159A65E-E809-46AD-8B44-6F927A4B0385');
INSERT INTO `sys_handler` VALUES ('439949F0-C6B7-49FF-8ED1-2A1B5062E7B9', 'SecurityGroupList', 'MAIN', '', '8C84B439-2788-4608-89C4-8F5AA076D124');
INSERT INTO `sys_handler` VALUES ('46C52D33-8797-4251-951F-F7CA23C76BD7', 'FunctionTreeManage', 'MAIN', null, 'C977BC31-C78F-4B16-B0C6-769783E46A06');
INSERT INTO `sys_handler` VALUES ('494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'SysLogQueryList', 'MAIN', null, '67BA273A-DD31-48D0-B78C-1D60D5316074');
INSERT INTO `sys_handler` VALUES ('88882DB9-967F-430E-BA9C-D0BBBBD2BD0C', 'ModifyPassword', 'MAIN', null, 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `sys_handler` VALUES ('89987D49-70FD-4AE6-B375-B67756D07EB2', 'EmEvectionManageEdit', 'OTHER', '', 'E269D481-2AD1-4C00-AE00-58CD687A8681');
INSERT INTO `sys_handler` VALUES ('9A16D554-F989-438A-B92D-C8C8AC6BF9B8', 'CodeTypeManageList', 'MAIN', null, 'A0334956-426E-4E49-831B-EB00E37285FD');
INSERT INTO `sys_handler` VALUES ('B4FE5722-9EA6-47D8-8770-D999A3F6A354', 'CodeListManageList', 'MAIN', null, '692B0D37-2E66-4E82-92B4-E59BCF76EE76');
INSERT INTO `sys_handler` VALUES ('E6F32D4A-51C5-440F-8062-CB743C0D5480', 'EmProConfigManageList', 'MAIN', '', '1159A65E-E809-46AD-8B44-6F927A4B0385');
INSERT INTO `sys_handler` VALUES ('FD79EC7E-B544-4D2D-B571-D66A583B0812', 'EmExpReimbursementManageList', 'MAIN', '', 'F2971ADA-620C-4BE2-B198-90D085632E57');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `ID` char(36) DEFAULT NULL,
  `OPER_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IP_ADDTRESS` varchar(32) DEFAULT NULL,
  `USER_ID` varchar(32) DEFAULT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `FUNC_NAME` varchar(64) DEFAULT NULL,
  `ACTION_TYPE` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('D3C8C2F6-612B-47D8-A7A4-B37695B10515', '2015-12-16 16:24:17', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('480B07DB-FF31-492B-82B7-BDFAB04A521E', '2015-12-16 16:24:58', '127.0.0.1', 'admin', '管理员', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('069C8272-9D17-4998-A66E-6C61902A190A', '2015-12-16 16:26:00', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('E6D82ED6-3634-4C17-BF09-94C4BD789491', '2015-12-16 16:26:57', '127.0.0.1', 'CS01', '张老大', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('B5373039-AE08-4E99-BEB0-A1128B6DD517', '2015-12-16 16:27:03', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('B4F79444-22A6-4C07-A597-E082ED527374', '2015-12-16 16:29:32', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('110A4430-6FB7-4698-AA90-201BE8C23CDB', '2015-12-16 16:29:58', '127.0.0.1', 'CS01', '张老大', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('90F65FDC-BEBF-47E9-AB02-B7881310E786', '2015-12-16 16:30:11', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('21D8CB21-BD39-4614-8369-940D0B0B1321', '2015-12-16 16:30:14', '127.0.0.1', 'CS01', '张老大', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('7EB1F122-43DC-47A7-BA34-226F66AAF177', '2015-12-16 16:30:26', '127.0.0.1', 'CS02', '赵小二', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('60561499-2847-4E4B-A252-BB168D88F665', '2015-12-16 16:30:44', '127.0.0.1', 'CS02', '赵小二', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('D3F4E01A-C3D1-4D53-A59D-71B860C0B728', '2015-12-16 16:30:54', '127.0.0.1', 'CS02', '赵小二', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('269DCFAE-46D3-4C2E-B95F-65702B66B725', '2015-12-16 16:30:56', '127.0.0.1', 'CS02', '赵小二', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('17357FAD-2A8A-4656-81ED-6E5839D8137A', '2015-12-16 16:31:06', '127.0.0.1', 'CS03', '孙小三', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('213C33F2-E385-4CF5-BF96-8376EAD4D228', '2015-12-16 16:31:20', '127.0.0.1', 'CS03', '孙小三', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('44D6EF52-D0E5-4909-BD3C-C7ED93D656BA', '2015-12-16 16:31:32', '127.0.0.1', 'CS03', '孙小三', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('AFD3F2A4-EF68-4ACE-B007-6CEED7D6B67C', '2015-12-16 16:31:34', '127.0.0.1', 'CS03', '孙小三', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('4BA27B57-AE5F-42E1-9BC5-774AD46F7F47', '2015-12-16 16:31:51', '127.0.0.1', 'CS04', '赵小四', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('F41AEEDA-7C9A-4451-9F7D-BE395FAD4828', '2015-12-16 16:32:11', '127.0.0.1', 'CS04', '赵小四', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('6234FBD6-C929-4995-9FB6-4715B9016025', '2015-12-16 16:32:20', '127.0.0.1', 'CS04', '赵小四', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('9D9AE8F1-3B05-4AD8-B8EF-B22FDE89D474', '2015-12-16 16:32:21', '127.0.0.1', 'CS04', '赵小四', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('994E3C33-9249-46B3-83D3-DE15220A48D0', '2015-12-16 16:32:32', '127.0.0.1', 'CS05', '翟小五', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('E9366115-52F8-4335-AC55-012183F090DA', '2015-12-16 16:32:47', '127.0.0.1', 'CS05', '翟小五', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('023002D3-7BAB-4B9D-8213-AE0156E790CD', '2015-12-16 16:33:18', '127.0.0.1', 'CS05', '翟小五', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('59E739EA-3122-4A8B-B79A-468C9B8358EA', '2015-12-16 16:33:20', '127.0.0.1', 'CS05', '翟小五', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('2A8FA36B-0267-4829-879B-A2729B3C500D', '2015-12-16 16:33:27', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('A0207710-DE8E-4E52-88C1-DAFEB60AE928', '2015-12-16 17:57:37', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('DEDD8E09-32B0-47BA-818D-1ACE4ED08AD7', '2015-12-16 18:10:46', '127.0.0.1', 'admin', '管理员', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('2532C92E-4DF0-4DBD-A937-20FE7C51BAFC', '2015-12-16 18:11:03', '127.0.0.1', 'CS02', '赵小二', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('45C9A4E7-8A76-4709-99D6-EC2D1CFB37AA', '2015-12-16 18:51:52', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('3AA7FDE5-B3D6-41D6-A901-C0A6B4ECB920', '2015-12-16 18:52:32', '127.0.0.1', 'admin', '管理员', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('1EACC9AD-DAE5-44E1-9222-678DE25E364B', '2015-12-16 18:52:42', '127.0.0.1', 'CS02', '赵小二', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('DF7DC5DA-EBD0-414D-9922-3E80BC6A1628', '2015-12-16 18:53:37', '127.0.0.1', 'CS02', '赵小二', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('FC4917B3-B5F0-47E2-97DE-67506EB6E87B', '2015-12-16 18:53:47', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('9FE7B2EB-95AF-4EF4-BACF-F84E3A57BF5E', '2015-12-16 18:53:58', '127.0.0.1', 'CS01', '张老大', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('C69B3985-4210-40E4-8836-C9DA797A1218', '2015-12-16 18:54:05', '127.0.0.1', 'CS02', '赵小二', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('AFF62596-AE90-48FE-810E-0876F3A6EC2A', '2015-12-16 18:54:21', '127.0.0.1', 'CS02', '赵小二', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('131B6A2E-8C52-42DC-8044-C9F09C95217A', '2015-12-16 18:54:28', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('59442CB0-3881-4D58-AF0D-C81C622AD9D0', '2015-12-16 18:54:39', '127.0.0.1', 'CS01', '张老大', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('5F0BAC7E-01F4-49F5-9DD7-33814EB31D9D', '2015-12-16 18:55:17', '127.0.0.1', 'CS02', '赵小二', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('F9BB7E25-2865-48C0-B85E-909027BB0B32', '2015-12-16 18:56:40', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('1ADCA191-52E3-458E-9867-68AACFCD3C65', '2015-12-16 19:00:06', '127.0.0.1', 'CS01', '张老大', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('81785515-7FAD-49BC-B49D-E1C177428B05', '2015-12-16 19:00:14', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('C69893AD-C21E-4729-B26F-23CA242E0BE5', '2015-12-16 19:00:19', '127.0.0.1', 'admin', '管理员', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('9B3CE82B-08D4-47AB-A417-5ED3131D3117', '2015-12-16 19:00:29', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('75A5BF6E-F9BE-4280-A6E0-6A0B4A8CF7AA', '2015-12-17 08:27:09', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('683246AA-6BBF-4570-B6F9-28E6B7B92086', '2015-12-17 08:29:46', '127.0.0.1', 'CS02', '赵小二', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('68369BA6-D94D-43FA-B3C2-99E615B4B6AC', '2015-12-17 08:30:32', '127.0.0.1', 'CS02', '赵小二', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('6874D91E-38D8-4ADA-A21A-2EE1FD57615E', '2015-12-17 08:30:43', '127.0.0.1', 'CS02', '赵小二', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('AFA283E9-6E7B-41E0-804E-FA8012CE85F0', '2015-12-17 09:20:37', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('A504417C-6BBD-4C66-A10F-95C21F1A8B6A', '2015-12-17 09:39:35', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('13A9D47A-8564-460E-AB2A-CC58FDBA3E26', '2015-12-17 09:39:41', '127.0.0.1', 'admin', '管理员', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('56B30E2B-D9BB-4D78-A32C-51373F8314F0', '2015-12-17 09:39:53', '127.0.0.1', 'CS02', '赵小二', '系统登陆', 'login');

-- ----------------------------
-- Table structure for sys_onlinecount
-- ----------------------------
DROP TABLE IF EXISTS `sys_onlinecount`;
CREATE TABLE `sys_onlinecount` (
  `IPADDRRESS` varchar(64) NOT NULL,
  `ONLINECOUNT` int(11) DEFAULT NULL,
  PRIMARY KEY (`IPADDRRESS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_onlinecount
-- ----------------------------

-- ----------------------------
-- Table structure for sys_operation
-- ----------------------------
DROP TABLE IF EXISTS `sys_operation`;
CREATE TABLE `sys_operation` (
  `OPER_ID` char(36) NOT NULL,
  `HANLER_ID` varchar(36) DEFAULT NULL,
  `OPER_CODE` varchar(64) DEFAULT NULL,
  `OPER_NAME` varchar(64) DEFAULT NULL,
  `OPER_ACTIONTPYE` varchar(64) DEFAULT NULL,
  `OPER_SORT` int(11) DEFAULT NULL,
  PRIMARY KEY (`OPER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_operation
-- ----------------------------
INSERT INTO `sys_operation` VALUES ('0DCE1544-7AFF-4523-82E0-660C46345A9A', '3666B5D3-C140-41B1-BCD2-C968E6627553', 'resubmit', 'resubmit', 'resubmit', '4');
INSERT INTO `sys_operation` VALUES ('32440E6B-8696-449E-AD39-E8D569E64DFB', 'FD79EC7E-B544-4D2D-B571-D66A583B0812', 'edit', 'edit', 'edit', '2');
INSERT INTO `sys_operation` VALUES ('3EC41FB8-ED98-41F3-B86D-F7473AEDD002', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'viewDetail', '查看', 'viewDetail', '1');
INSERT INTO `sys_operation` VALUES ('40091263-6996-4171-B33E-DADE8FE17AA8', 'FD79EC7E-B544-4D2D-B571-D66A583B0812', 'detail', 'detail', 'detail', '3');
INSERT INTO `sys_operation` VALUES ('636CD720-2D1E-4B16-878F-AE84CAF23E48', 'FD79EC7E-B544-4D2D-B571-D66A583B0812', 'delete', 'delete', 'delete', '5');
INSERT INTO `sys_operation` VALUES ('6BF7A157-D333-4F40-8612-F61D3FD4D258', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'refreshImgBtn', '刷新', 'refresh', '2');
INSERT INTO `sys_operation` VALUES ('73D4D092-7D3C-4738-ACAD-6DDC6325DFF9', '3666B5D3-C140-41B1-BCD2-C968E6627553', 'submit', 'submit', 'submit', '3');
INSERT INTO `sys_operation` VALUES ('84DE12F3-1422-4BBC-8899-3B2DED5B95F3', '3666B5D3-C140-41B1-BCD2-C968E6627553', 'modify', 'modify', 'modify', '1');
INSERT INTO `sys_operation` VALUES ('8BB5C7DC-4034-44DB-A6C0-D07D066290F2', '3666B5D3-C140-41B1-BCD2-C968E6627553', 'goBack', 'goBack', 'goBack', '5');
INSERT INTO `sys_operation` VALUES ('9D6C0E22-F85E-4587-B714-E3DAF089C89C', '081B8F8A-7BAD-434B-A34D-4404A1E34D21', 'modify', 'modify', 'modify', '1');
INSERT INTO `sys_operation` VALUES ('A43098DE-C438-42EE-BB53-297545655162', 'E6F32D4A-51C5-440F-8062-CB743C0D5480', 'delete', 'delete', 'delete', '4');
INSERT INTO `sys_operation` VALUES ('AFEF0A41-70CF-449C-A276-2E31CF91AC29', 'E6F32D4A-51C5-440F-8062-CB743C0D5480', 'create', 'create', 'create', '1');
INSERT INTO `sys_operation` VALUES ('B55F6FFD-A811-4914-96C0-9ACDC88CBCC6', '081B8F8A-7BAD-434B-A34D-4404A1E34D21', 'save', 'save', 'save', '2');
INSERT INTO `sys_operation` VALUES ('CF0B875E-2AFE-4C79-80BE-ACAFC7FBF597', 'FD79EC7E-B544-4D2D-B571-D66A583B0812', 'flow', 'flow', 'flow', '4');
INSERT INTO `sys_operation` VALUES ('E0AF7BA5-1257-47F2-AF75-DBC058030635', '081B8F8A-7BAD-434B-A34D-4404A1E34D21', 'goBack', 'goBack', 'goBack', '5');
INSERT INTO `sys_operation` VALUES ('E3FA279D-2C2B-4C25-B9C8-9F80366E9CEA', 'E6F32D4A-51C5-440F-8062-CB743C0D5480', 'detail', 'detail', 'detail', '3');
INSERT INTO `sys_operation` VALUES ('E5495879-6FB8-405C-96C3-5F23512EA721', 'FD79EC7E-B544-4D2D-B571-D66A583B0812', 'create', 'create', 'create', '1');
INSERT INTO `sys_operation` VALUES ('E74DA9D6-768F-4768-A6CD-9BC64185DB4F', 'E6F32D4A-51C5-440F-8062-CB743C0D5480', 'edit', 'edit', 'edit', '2');
INSERT INTO `sys_operation` VALUES ('E8C98FE2-C52E-4FF8-AEDC-36F531BDD7B3', '081B8F8A-7BAD-434B-A34D-4404A1E34D21', 'flow', 'flow', 'flow', '4');
INSERT INTO `sys_operation` VALUES ('F1A33184-ADC4-4C26-9A65-A36693599C3E', '3666B5D3-C140-41B1-BCD2-C968E6627553', 'save', 'save', 'save', '2');
INSERT INTO `sys_operation` VALUES ('F656CD82-E71D-434F-B487-C3A60BAD2714', '081B8F8A-7BAD-434B-A34D-4404A1E34D21', 'submit', 'submit', 'submit', '3');

-- ----------------------------
-- Table structure for wcm_general_group
-- ----------------------------
DROP TABLE IF EXISTS `wcm_general_group`;
CREATE TABLE `wcm_general_group` (
  `GRP_ID` varchar(36) NOT NULL,
  `GRP_NAME` varchar(64) DEFAULT NULL,
  `GRP_PID` varchar(36) DEFAULT NULL,
  `GRP_ORDERNO` int(11) DEFAULT NULL,
  `GRP_IS_SYSTEM` varchar(32) DEFAULT NULL,
  `GRP_RES_TYPE_DESC` varchar(32) DEFAULT NULL,
  `GRP_RES_TYPE_EXTS` varchar(128) DEFAULT NULL,
  `GRP_RES_SIZE_LIMIT` varchar(32) DEFAULT NULL,
  `GRP_DESC` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`GRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wcm_general_group
-- ----------------------------
INSERT INTO `wcm_general_group` VALUES ('77777777-7777-7777-7777-777777777777', '附件目录', '', null, '', '', '', '', '');
INSERT INTO `wcm_general_group` VALUES ('A6018D88-8345-46EE-A452-CE362FAC72E2', '视频文件', '77777777-7777-7777-7777-777777777777', '1', 'Y', '视频', '*.mp4;*.3gp;*.wmv;*.avi;*.rm;*.rmvb;*.flv', '100MB', '');
INSERT INTO `wcm_general_group` VALUES ('CF35D1E6-102E-428A-B39C-0072D491D5B1', '业务附件', '77777777-7777-7777-7777-777777777777', '2', 'Y', '所有资源', '*.*', '2MB', '');

-- ----------------------------
-- Table structure for wcm_general_resource
-- ----------------------------
DROP TABLE IF EXISTS `wcm_general_resource`;
CREATE TABLE `wcm_general_resource` (
  `RES_ID` varchar(36) NOT NULL,
  `GRP_ID` varchar(36) DEFAULT NULL,
  `RES_NAME` varchar(64) DEFAULT NULL,
  `RES_SHAREABLE` varchar(32) DEFAULT NULL,
  `RES_LOCATION` varchar(256) DEFAULT NULL,
  `RES_SIZE` varchar(64) DEFAULT NULL,
  `RES_SUFFIX` varchar(32) DEFAULT NULL,
  `RES_DESCRIPTION` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`RES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wcm_general_resource
-- ----------------------------
